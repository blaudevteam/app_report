import os, sys

PROJECT_DIR = '/home/ubuntu/apps/app_report'
sys.path.insert(0, PROJECT_DIR)


def execfile(filename):
    globals = dict( __file__ = filename )
    exec( open(filename).read(), globals )

activate_this = os.path.join( PROJECT_DIR, 'venv3/bin', 'activate_this.py' )
execfile( activate_this )


from web_app import app as application
application.debug = True
