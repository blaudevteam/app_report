#################
#### imports ####
#################
from flask import Flask, render_template, make_response, jsonify
from flask_bcrypt import Bcrypt
from flask_pymongo import PyMongo

from web_app.bin.utils.utils import get_app_base_path
from web_app.config import configure_app

################
#### config ####
################

app = Flask(__name__,
            instance_path=get_app_base_path(),
            instance_relative_config=True,
            template_folder='templates')

configure_app(app)
conn = PyMongo(app)
bcrypt = Bcrypt(app)

####################
#### blueprints ####
####################

from web_app.bin.user.views import user_blueprint
from web_app.bin.project.views import project_blueprint
from web_app.bin.client.views import client_blueprint
app.register_blueprint(user_blueprint)
app.register_blueprint(project_blueprint)
app.register_blueprint(client_blueprint)



############################
#### custom error pages ####
###############A#############


@app.after_request
def add_header(response):
    # response.cache_control.no_store = True
    response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '-1'
    return response


@app.errorhandler(400)
def page_not_found(e):
    return make_response(jsonify({'error': 'Not found'}), 400)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('/views/errors/404.html'), 404


@app.errorhandler(403)
def page_not_found(e):
    return render_template('/views/errors/403.html'), 403


@app.errorhandler(410)
def page_not_found(e):
    return render_template('/views/errors/410.html'), 410
