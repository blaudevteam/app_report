# coding=utf-8
from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,DateField,SelectField,FileField
from wtforms.validators import DataRequired


class CreateNewClient(FlaskForm):
    group = StringField('Grupo*', validators=[DataRequired(message='Debe ingresar el grupo')])
    name = StringField('Nombre*', validators=[DataRequired(message='Debe ingresar el nombre')])
    email = StringField('Correo*', validators=[DataRequired(message='Debe ingresar el correo')])
    user = StringField('Usuario*', validators=[DataRequired(message='Debe ingresar el usuario')])
    password = PasswordField('Clave*', validators=[DataRequired(message='Debe ingresar la clave')])
    new_password = PasswordField('Nueva clave*', validators=[DataRequired(message='Debe ingresar la nueva clave')])
    flag_password = StringField('Flag clav*', validators=[])
    company = StringField('Empresa*', validators=[DataRequired(message='Debe ingresar la empresa')])

class CreateNewCompany(FlaskForm):
    name = StringField('Nombre*', validators=[DataRequired(message='Debe ingresar el nombre')])

class CreateNewGroup(FlaskForm):
    name = StringField('Nombre*', validators=[DataRequired(message='Debe ingresar el nombre')])

class NewAssignment(FlaskForm):
    group = StringField('Grupo*', validators=[DataRequired(message='Debe ingresar el grupo')])
    project = StringField('Proyecto*', validators=[DataRequired(message='Debe ingresar el proyecto')])

class NewSprintClient(FlaskForm):
    group = StringField('Grupo*', validators=[DataRequired(message='Debe ingresar el nombre')])

class AddSprintForm(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(message='Debe ingresar el nombre')])
    date = DateField('Fecha Estimada',format='%Y-%m-%d' ,validators=[DataRequired(message='Debe completar el formato correcto')])
    time = StringField('Horas Estimadas', validators=[DataRequired(message='Debe ingresar las horas')])
    state = SelectField('Estado:',choices=[('Pendiente', 'Pendientes'),('En proceso', 'En proceso')])
    sprint = StringField('Sprint', validators=[])
    journeys = StringField('Jornadas', validators=[DataRequired(message='Debe ingresar las jornadas')])
    priority = SelectField('Estado:',choices=[('baja', 'Baja'),('media', 'Media'),('alta', 'Alta')])
    explotation = SelectField('Tipo explotacion:',choices=[('reporte', 'Reporte'),('graphite', 'Graphite'),('dashboard', 'Dashboard'),('desarrollo', 'Desarrollo')])
    description = StringField('Descripcion', validators=[])

class FormPhotoProfile(FlaskForm):
    photo = FileField('Imagen*', validators=[DataRequired(message='Debe ingresar la imagen')])

class NewDocumentModule(FlaskForm):
    document = FileField('Documento*', validators=[DataRequired(message='Debe ingresar el documento')])
