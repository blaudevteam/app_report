import random
import pandas as pd
from web_app import bcrypt,app
from datetime import date
from datetime import datetime
from bson import ObjectId
from flask import session


class ClientDao:
    def __init__(self, conn):
        self.db_client = conn

    def add_client(self,user_name=None,name=None,email=None,company=None,group=None,password=None,img_client=None):
        client = self.db_client.db.clientes.find_one({"user_name": user_name,"activo":True})
        user = self.db_client.db.usuarios.find_one({"user_name": user_name})

        if (client or user):
            return True
        else:
            self.db_client.db.clientes.insert({
            "user_name": user_name
            ,"nombre":name
            ,"email": email
            , "active": True
            , "empresa": company
            , "grupo": group
            , "rol": 'cliente'
            , "img": img_client
            ,"password": bcrypt.generate_password_hash(password)})

            return False

    def validate_client_exist(self,user_name):
        user = self.db_client.db.clientes.find_one({"user_name": user_name})
        if user:
            return True
        else:
            return False

    def get_clients(self,name=None,user=None,offset=0,items_pages=0):
        query = self.__build_query(name,user)
        count_client = self.get_client_count(name,user)
        clients_db = self.db_client.db.clientes.find(query).skip(offset).limit(items_pages)
        clients = [{"id": str(file_rec['_id'])
                  ,"usuario": file_rec['user_name']
                  ,"nombre": file_rec['nombre']
                  ,"email": file_rec['email']
                  ,"empresa": file_rec['empresa']
                  ,"grupo": file_rec['grupo']
                  ,"active": file_rec['active']
                  } for file_rec in clients_db]

        df_clients=pd.DataFrame()
        if (count_client > 0):
            df_clients = pd.DataFrame(clients)
            df_clients['nombre_empresa'] = df_clients.apply(lambda x: self.__get_name_company(x), axis=1)
            df_clients['nombre_grupo'] = df_clients.apply(lambda x: self.__get_name_group(x), axis=1)

        return df_clients.to_dict('records')

    def get_client_count(self,name=None,user=None):
        query = self.__build_query(name,user)
        return self.db_client.db.clientes.find(query).count()

    def get_client(self,id):
        client_db = self.db_client.db.clientes.find_one({"_id": ObjectId(id)})
        client = {"id": client_db['_id']
                ,"usuario": client_db['user_name']
                ,"nombre": client_db['nombre']
                ,"email": client_db['email']
                ,"password": client_db['password']
                ,"empresa": client_db['empresa']
                ,"grupo": client_db['grupo']
                ,"active": client_db['active']
                }
        return client

    def update_client(self,id,name,email,company,group):
        self.db_client.db.clientes.update_one({"_id": ObjectId(id)}, {"$set": {"nombre": name, "email": email
        , "empresa": company,"grupo":group}})

    def delete_client(self,id):
        self.db_client.db.clientes.update_one({"_id": ObjectId(id)}, {"$set": {"active": False}})

    def get_companies(self):
        companies_db = self.db_client.db.empresas.find({"activo":True})
        companies = [{"id": str(file_rec['_id'])
                  ,"nombre": file_rec['nombre']
                  ,"activo": file_rec['activo']
                  } for file_rec in companies_db]
        return companies

    def get_companies_count(self):
        return self.db_client.db.empresas.find({"activo":True}).count()

    def get_companies_filter(self,name=None,offset=0,items_pages=0):
        query = self.__build_query_companies(name)
        companies_db = self.db_client.db.empresas.find(query).skip(offset).limit(items_pages)
        companies = [{"id": str(file_rec['_id'])
                  ,"nombre": file_rec['nombre']
                  ,"activo": file_rec['activo']
                  } for file_rec in companies_db]
        return companies

    def get_companies_count_filter(self,name=None):
        query = self.__build_query_companies(name)
        return self.db_client.db.empresas.find(query).count()

    def get_companies_unfiltered(self):
        companies_db = self.db_client.db.empresas.find({})
        companies = [{"id": str(file_rec['_id'])
                  ,"nombre": file_rec['nombre']
                  ,"activo": file_rec['activo']
                  } for file_rec in companies_db]
        return companies

    def get_company(self,id):
        company_db = self.db_client.db.empresas.find_one({"_id": ObjectId(id)})
        company = {"id": company_db['_id']
                ,"nombre": company_db['nombre']
                }
        return company

    def edit_company(self,id,name):
        company = self.db_client.db.empresas.find_one({"nombre": name,"activo":True})

        if (company):
            return True
        else:

            self.db_client.db.empresas.update_one({"_id": ObjectId(id)}, {"$set": {"nombre": name}})

            return False

    def delete_company(self,id):
        self.db_client.db.empresas.update_one({"_id": ObjectId(id)}, {"$set": {"activo": False}})

    def add_company(self,name):

        company = self.db_client.db.empresas.find_one({"nombre": name,"activo":True})

        if (company):
            return True
        else:
            self.db_client.db.empresas.insert({
            "nombre": name
            ,"activo":True})

            return False

    def get_groups(self):
        group_db = self.db_client.db.grupos.find({"activo":True})
        groups = [{"id": str(file_rec['_id'])
                  ,"nombre": file_rec['nombre']
                  ,"activo": file_rec['activo']
                  } for file_rec in group_db]
        return groups

    def get_groups_count(self):
        return self.db_client.db.grupos.find({"activo":True}).count()

    def get_groups_unfiltered(self):
        group_db = self.db_client.db.grupos.find({})
        groups = [{"id": str(file_rec['_id'])
                  ,"nombre": file_rec['nombre']
                  ,"activo": file_rec['activo']
                  } for file_rec in group_db]
        return groups

    def add_group(self,name):
        group = self.db_client.db.grupos.find_one({"nombre": name,"activo":True})

        if (group):
            return True
        else:
            self.db_client.db.grupos.insert({
            "nombre": name
            ,"activo":True})

            return False

    def get_groups_filter(self,name=None,offset=0,items_pages=0):
        query = self.__build_query_groups(name)
        groups_db = self.db_client.db.grupos.find(query).skip(offset).limit(items_pages)
        groups = [{"id": str(file_rec['_id'])
                  ,"nombre": file_rec['nombre']
                  ,"activo": file_rec['activo']
                  } for file_rec in groups_db]
        return groups

    def get_groups_filter_count(self,name=None):
        query = self.__build_query_groups(name)
        return self.db_client.db.grupos.find(query).count()

    def get_group(self,id):
        group_db = self.db_client.db.grupos.find_one({"_id": ObjectId(id)})
        group = {"id": group_db['_id']
                ,"nombre": group_db['nombre']
                }
        return group

    def edit_group(self,id,name):
        group = self.db_client.db.grupos.find_one({"nombre": name,"activo":True})

        if (group):
            return True
        else:

            self.db_client.db.grupos.update_one({"_id": ObjectId(id)}, {"$set": {"nombre": name}})

            return False

    def delete_group(self,id):
        self.db_client.db.grupos.update_one({"_id": ObjectId(id)}, {"$set": {"activo": False}})
        count_assignments =  self.db_client.db.asignaciones.find({"grupo":id,"activo":True}).count()
        if (count_assignments > 0):
            assignment_db = self.db_client.db.asignaciones.find({"grupo":id,"activo":True})
            assignments = [{"id": str(file_rec['_id'])} for file_rec in assignment_db]
            df_assignments = pd.DataFrame(assignments)
            df_assignments['eliminacion'] = df_assignments.apply(lambda x: self.__delete_assignment(x), axis=1)

    def new_assign_client(self,group,project):
        assignment = self.db_client.db.asignaciones.find_one({"grupo": group,"proyecto":project,"activo":True})

        if (assignment):
            return True
        else:
            self.db_client.db.asignaciones.insert({
            "grupo": group
            ,"proyecto":project
            ,"ticket":True
            ,"activo":True
            })
            return False

    def get_assignments(self,group=None,project=None,offset=0,items_pages=0):
        query = self.__build_query_assignment(group,project)
        assignments_db = self.db_client.db.asignaciones.find(query).skip(offset).limit(items_pages)
        assignments = [{"id": str(file_rec['_id'])
                  ,"grupo": file_rec['grupo']
                  ,"proyecto": file_rec['proyecto']
                  ,"ticket": file_rec['ticket']
                  ,"activo": file_rec['activo']
                  } for file_rec in assignments_db]

        df_assignments = pd.DataFrame()
        count_assignments = self.get_assignments_count(group,project)

        if (count_assignments > 0):
            df_assignments = pd.DataFrame(assignments)
            df_assignments['nombre_grupo'] = df_assignments.apply(lambda x: self.__get_name_group(x), axis=1)
            df_assignments['nombre_proyecto'] = df_assignments.apply(lambda x: self.__get_name_project(x), axis=1)

        return df_assignments.to_dict('records')

    def get_assignments_count(self,group=None,project=None):
        query = self.__build_query_assignment(group,project)
        app.logger.debug('query %s',query)
        return self.db_client.db.asignaciones.find(query).count()

    def get_assignment(self,id):
        assignment_db = self.db_client.db.asignaciones.find_one({"_id": ObjectId(id)})
        assignment = {"id": assignment_db['_id']
                ,"grupo": assignment_db['grupo']
                ,"proyecto": assignment_db['proyecto']
                }
        return assignment

    def update_assign_client(self,id,group,project):
        assignment = self.db_client.db.asignaciones.find_one({"grupo": group,"proyecto":project,"activo":True})

        if (assignment):
            return True
        else:
            self.db_client.db.asignaciones.update_one({"_id": ObjectId(id)}, {"$set": {"grupo": group,"proyecto":project}})
            return False

    def delete_assignment(self,id):
        self.db_client.db.asignaciones.update_one({"_id": ObjectId(id)}, {"$set": {"activo": False}})

    def update_active_ticket(self,id,active):
        self.db_client.db.asignaciones.update_one({"_id": ObjectId(id)}, {"$set": {"ticket": active}})

    def get_projects(self,id_client,offset=0,items_pages=0):
        client = self.get_client(id_client)
        df_projects = pd.DataFrame()
        count_assignments = self.db_client.db.asignaciones.find({"grupo":client['grupo'],"activo":True}).count()
        if (count_assignments > 0):
            assignments_db = self.db_client.db.asignaciones.find({"grupo":client['grupo'],"activo":True}).skip(offset).limit(items_pages)
            assignments = [{"id": str(file_rec['_id']),"grupo": file_rec['grupo']
                      ,"proyecto": file_rec['proyecto']
                      ,"ticket": file_rec['ticket']
                      } for file_rec in assignments_db]
            df_projects = pd.DataFrame(assignments)
            df_projects['nombre_proyecto'] = df_projects.apply(lambda x: self.__get_name_project(x), axis=1)

        return df_projects.to_dict('records')

    def get_projects_count(self,id_client):
        client = self.get_client(id_client)
        df_projects = pd.DataFrame()
        return self.db_client.db.asignaciones.find({"grupo":client['grupo'],"activo":True}).count()

    def get_my_sprints(self,id_client,offset=0,items_pages=0):
        task_db = self.db_client.db.sprints.find({"cliente":id_client,"activo":True}).skip(offset).limit(items_pages)
        tasks =[{
            "id": str(file_rec['_id']),
            "nombre": file_rec['nombre'],
            "estado": file_rec['estado'],
            "fecha_estimada": file_rec['fecha_estimada'].strftime("%d-%m-%Y"),
            "fecha_fin": file_rec['fecha_fin'].strftime("%d-%m-%Y"),
            "horas_estimadas": file_rec['horas_estimadas'],
            "id_proyecto": file_rec['id_proyecto'],
            "horas_fin": file_rec['horas_fin'],
            "flag_cliente": file_rec['flag_cliente'],
            "jornada": file_rec['jornada'],
            "sprint": file_rec['sprint'],
            "activo": file_rec['activo']
        } for file_rec in task_db]
        count_modules = self.get_count_my_sprints(id_client)
        df_modules = pd.DataFrame(tasks)
        df_modules['horas_invertidas'] = 0
        if (count_modules > 0):
            df_modules['nombre_sprint'] = df_modules.apply(lambda x: self.__get_name_sprint(x), axis=1)
            df_modules['horas_invertidas'] = df_modules.apply(lambda x: self.get_hours_inverted_module(x), axis=1)
        return df_modules.to_dict('records')

    def get_count_my_sprints(self,id_client):
        return self.db_client.db.sprints.find({"cliente":id_client,"activo":True}).count()


    def update_img_client(self,new_img,id):
         self.db_client.db.clientes.update_one({"_id": ObjectId(id)}
         ,{"$set": {"img":new_img}})
         return True

    def match_user_module(self,id_module,id_user):
        app.logger.debug('modulo %s',id_module)
        app.logger.debug('usuario %s',id_user)
        match = self.db_client.db.developers_asignados.find({"id_sprint":id_module,"id_usuario":id_user,"activo":True}).count()
        if (match > 0):
            return True
        else:
            return False

    def update_password_client(self,id,password,new_pass):
        client = self.get_client(id)
        if bcrypt.check_password_hash(client["password"], password):

             self.db_client.db.clientes.update_one({"_id": ObjectId(id)}
             ,{"$set": {"password": bcrypt.generate_password_hash(new_pass)}})

             return True
        else:
            return False

    def __build_query(self,name=None,user=None):
        query = {"active":True}

        if name:
            query["nombre"] = {"$regex": name, "$options": 'i'}
        if user:
            query["user_name"] = {"$regex": user, "$options": 'i'}

        return query

    def __build_query_companies(self,name=None):
        query = {"activo":True}

        if name:
            query["nombre"] = {"$regex": name, "$options": 'i'}

        return query

    def __build_query_groups(self,name=None):
        query = {"activo":True}

        if name:
            query["nombre"] = {"$regex": name, "$options": 'i'}

        return query

    def __build_query_assignment(self,group=None,project=None):
        query = {"activo":True}

        if group:
            query["grupo"] = {"$regex": group, "$options": 'i'}

        if project:
            query["proyecto"] = {"$regex": project, "$options": 'i'}

        return query

    def __get_name_company(self,row):
        company_db = self.db_client.db.empresas.find_one({"_id": ObjectId(row['empresa'])})
        company = {"nombre": company_db['nombre']}
        return company['nombre']

    def __get_name_group(self,row):
        group_db = self.db_client.db.grupos.find_one({"_id": ObjectId(row['grupo'])})
        group = {"nombre": group_db['nombre']}
        return group['nombre']

    def __get_name_project(self,row):
        project_db = self.db_client.db.proyectos.find_one({"_id": ObjectId(row['proyecto'])})
        project = {"nombre": project_db['nombre']}
        return project['nombre']


    def __delete_assignment(self,row):
        self.db_client.db.asignaciones.update_one({"_id": ObjectId(str(row['id']))}, {"$set": {"activo": False}})
        return '---'

    def __get_name_sprint(self,row):
        name = 'No configurado'
        if (str(row['sprint'])):
            sprint_db = self.db_client.db.sprints_proyecto.find_one({"_id": ObjectId(str(row['sprint']))})
            sprint = {"nombre": sprint_db['nombre']}
            name = sprint['nombre']
        return name

    def get_hours_inverted_module(self,row):
        total = 0
        task_count = self.db_client.db.tareas.find({"id_sprint":str(row['id']),"activo":True}).count()
        if (task_count > 0):
            total = 0
            task_db = self.db_client.db.tareas.find({"id_sprint":str(row['id']),"activo":True})
            tasks = [{"horas_invertidas": file_rec['horas_invertidas']} for file_rec in task_db]

            for task in tasks:
                total += int(task['horas_invertidas'])

        return total
