# coding=utf-8
import json
from datetime import date
from datetime import datetime
from flask import render_template, Blueprint, request, session, redirect, url_for,send_file
from flask_paginate import get_page_parameter, Pagination
from flask_login import LoginManager, login_required, logout_user, login_user
from werkzeug.utils import secure_filename
import os

from .forms import CreateNewClient,CreateNewCompany,CreateNewGroup,NewAssignment,AddSprintForm,FormPhotoProfile,NewDocumentModule

from web_app import app, conn
from web_app.bin.client.models import ClientDao
from web_app.bin.project.models import ProjectDao
from web_app.bin.user.models import UserDao


items_for_page = app.config["ITEMS_FOR_PAGE"]
#######################
#### Configuration ####
#######################



client_blueprint = Blueprint('client', __name__)

user_dao = UserDao(conn)
client_dao = ClientDao(conn)
project_dao = ProjectDao(conn)


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


################
#### routes ####
################




@client_blueprint.route('/homec', methods=['GET', 'POST'])
@login_required
def homec():


    return render_template('views/client/homec.html')

@client_blueprint.route('/projects_client', methods=['GET', 'POST'])
@login_required
def projects_client():

    if session['rol'] != 'cliente' :
        return redirect(url_for('user.home'))

    client_id = session['id_user']

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    projects = client_dao.get_projects(client_id,offset,items_for_page)
    projects_count = client_dao.get_projects_count(client_id)



    pagination = Pagination(css_framework='bootstrap3', page=page, total=projects_count,
                            per_page_parameter=items_for_page,record_name='projects')

    return render_template('views/client/projects_client.html',projects=projects
    ,pagination=pagination)

@client_blueprint.route('/new_sprint_client/<id_project>', methods=['GET', 'POST'])
@login_required
def new_sprint_client(id_project):
    info = None
    error = None
    if session['rol'] != 'cliente' :
        return redirect(url_for('user.home'))

    form = AddSprintForm(request.form)

    sprints = project_dao.get_sprints_project(id_project)
    count_sprints_project = project_dao.get_sprints_project_count(id_project)



    if request.method == 'POST':
        project = project_dao.get_project(id_project)
        id_client = session['id_user']

        project_dao.add_sprint(id_project,form.name.data,'Pendiente',form.date.data,0,form.sprint.data,form.priority.data,form.description.data
        ,True,id_client,form.explotation.data,0,id_client,0)

        if (form.sprint.data == ''):
            message = '{} agrego el sprint: {} al proyecto: {} (no tiene sprint)'.format(session['id'],form.name.data,project['nombre'])
        else:
            message = '{} agrego el sprint: {} al proyecto: {}'.format(session['id'],form.name.data,project['nombre'])

        name = 'Nuevo modulo (CLIENTE)'
        user_dao.add_notification(name,message,to_admin = True,id_admin=project['admin'])


        info = 'Se agrego correctamente el sprint'

    return render_template('views/client/new_sprint_client.html',form=form,info=info,error=error
    ,count_sprints_project=count_sprints_project,sprints_project=sprints)

@client_blueprint.route('/my_sprints', methods=['GET', 'POST'])
@login_required
def my_sprints():

    id_client = session['id_user']

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    my_sprints = client_dao.get_my_sprints(id_client,offset,items_for_page)
    count_my_sprints = client_dao.get_count_my_sprints(id_client)


    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_my_sprints,
                            per_page_parameter=items_for_page,record_name='my_sprints')

    return render_template('views/client/my_sprints.html',my_sprints=my_sprints,pagination=pagination)



@client_blueprint.route('/new_client', methods=['GET', 'POST'])
@login_required
def new_client():
    info = None
    error = None
    form = CreateNewClient(request.form)

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    if request.method == 'POST':
        result = client_dao.add_client(form.user.data,form.name.data,form.email.data,form.company.data,form.group.data,form.password.data,'avatar2.png')

        if (result):
            error = ' El usuario ya existe'
        else:
            info = ' El cliente {} fue creado exitosamente'.format(form.name.data)
            return redirect(url_for('client.clients',info=info))

    companies = client_dao.get_companies()
    groups = client_dao.get_groups()
    count_companies = client_dao.get_companies_count()
    count_groups = client_dao.get_groups_count()
    return render_template('views/client/new_client.html',form=form,count_notifications=count_notifications
    ,count_projects=count_projects,info=info,error=error,companies=companies,groups=groups
    ,count_companies=count_companies,count_groups=count_groups)

@client_blueprint.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():

    info = None
    error = None
    form = CreateNewClient(request.form)

    if request.method == 'POST' :
        if (form.flag_password.data=="1"):
            result = client_dao.update_password_client(session['id_user'],form.password.data,form.new_password.data)
            if (result):
                info = 'Se han actualizado los datos'
            else:
                error = 'Las claves no coinciden'
    return render_template('views/client/edit_profile.html',form=form,info=info,error=error)

@client_blueprint.route('/upload_client_photo', methods=['GET', 'POST'])
@login_required
def upload_client_photo():
    info = None
    error = None
    form = FormPhotoProfile(request.form)

    if request.method == 'POST'  :
        if 'photo' not in request.files:
            error ='El arhivo no existe'
            return render_template('views/client/upload_client_photo.html',form=form,info=info,error=error)


        file = request.files['photo']
        if file.filename == '':
            error = 'Archivo no seleccionado'
            return render_template('views/client/upload_client_photo.html',form=form,info=info,error=error)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            client_dao.update_img_client(filename,session['id_user'])
            info = 'Se ha actualizado tu foto correctamente, debes reiniciar tu sesion para notar los cambios'
        else:
            error = 'El archivo no es compartible'

    return render_template('views/client/upload_client_photo.html',form=form,info=info,error=error)

@client_blueprint.route('/clients', methods=['GET', 'POST'])
@login_required
def clients():
    form = CreateNewClient(request.form)
    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))
    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    info = request.args.get('info',None)
    error = request.args.get('error',None)

    if request.method == 'POST':
        try:
            name = form.name.data
            user = form.user.data
            href = url_for("client.clients", name=name,user=user)
            href += "&page={0}"
        except Exception as e:
            print(e)
    else:
        name = request.args.get("name", '')
        user = request.args.get("user", '')
        href = url_for("client.clients", name=name,user=user)
        href += "&page={0}"

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    clients = client_dao.get_clients(name,user,offset,items_for_page)
    count_file_clients = client_dao.get_client_count(name,user)

    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_file_clients,
                            per_page_parameter=items_for_page,record_name='clients',href=href)

    return render_template('views/client/clients.html',count_notifications=count_notifications
    ,count_projects=count_projects,pagination=pagination,clients=clients,form=form,info=info,error=error)

@client_blueprint.route('/edit_client/<client_id>', methods=['GET', 'POST'])
@login_required
def edit_client(client_id):
    info=None
    error=None
    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))
    form = CreateNewClient(request.form)
    client = client_dao.get_client(client_id)

    if request.method == 'POST' :
        client_dao.update_client(client_id,form.name.data,form.email.data,form.company.data,form.group.data)
        info = 'Cliente actualizado'
    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    companies = client_dao.get_companies_unfiltered()
    groups = client_dao.get_groups_unfiltered()

    return render_template('views/client/edit_client.html',form=form,client=client
    ,info=info,error=error,count_notifications=count_notifications,count_projects=count_projects
    ,companies=companies,groups=groups)

@client_blueprint.route('/new_company', methods=['GET', 'POST'])
@login_required
def new_company():

    form = CreateNewCompany(request.form)

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    info = None
    error = None

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST' and form.validate_on_submit():
        result = client_dao.add_company(form.name.data)
        if (result):
            error = 'La empresa ya se encuentra creada'
        else:
            info = 'La empresa {} se agrego con exito'.format(form.name.data)
            return redirect(url_for('client.companies',info=info,error=error))

    return render_template('views/company/new_company.html',count_projects=count_projects
    ,count_notifications=count_notifications,info=info,error=error,form=form)

@client_blueprint.route('/companies', methods=['GET', 'POST'])
@login_required
def companies():

    form = CreateNewCompany(request.form)

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    info = request.args.get('info',None)
    error = request.args.get('error',None)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST':
        try:
            name = form.name.data
            href = url_for("client.companies", name=name)
            href += "&page={0}"
        except Exception as e:
            print(e)
    else:
        name = request.args.get("name", '')
        href = url_for("client.companies", name=name)
        href += "&page={0}"

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    companies = client_dao.get_companies_filter(name,offset,items_for_page)
    count_companies = client_dao.get_companies_count_filter(name)

    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_companies,
                            per_page_parameter=items_for_page,record_name='companies')

    return render_template('views/company/companies.html',companies=companies,count_projects=count_projects
    ,count_notifications=count_notifications,pagination=pagination,form=form,info=info,error=error)

@client_blueprint.route('/edit_company/<company_id>', methods=['GET', 'POST'])
@login_required
def edit_company(company_id):

    info = None
    error = None

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    form = CreateNewCompany(request.form)

    company = client_dao.get_company(company_id)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST' :
        result = client_dao.edit_company(company_id,form.name.data)
        if(result):
            error = 'Esta empresa ya esta creada'
        else:
            info = 'Se actualizaron los datos de la empresa'

    return render_template('views/company/edit_company.html',form=form,info=info,error=error,company=company
    ,count_projects=count_projects,count_notifications=count_notifications)


@client_blueprint.route('/new_group', methods=['GET', 'POST'])
@login_required
def new_group():

    form = CreateNewGroup(request.form)

    info = None
    error = None

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST' and form.validate_on_submit():
        result = client_dao.add_group(form.name.data)
        if (result):
            error = 'El grupo ya existe'
        else:
            info = 'El grupo {} se agrego con exito'.format(form.name.data)
            return redirect(url_for('client.groups',info=info,error=error))

    return render_template('views/group/new_group.html',count_projects=count_projects
    ,count_notifications=count_notifications,info=info,error=error,form=form)

@client_blueprint.route('/groups', methods=['GET', 'POST'])
@login_required
def groups():

    form = CreateNewGroup(request.form)

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    info = request.args.get('info',None)
    error = request.args.get('error',None)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST':
        try:
            name = form.name.data
            href = url_for("client.groups", name=name)
            href += "&page={0}"
        except Exception as e:
            print(e)
    else:
        name = request.args.get("name", '')
        href = url_for("client.groups", name=name)
        href += "&page={0}"

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    groups = client_dao.get_groups_filter(name,offset,items_for_page)
    count_groups = client_dao.get_groups_filter_count(name)

    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_groups,
                            per_page_parameter=items_for_page,record_name='groups',href=href)

    return render_template('views/group/groups.html',groups=groups,count_projects=count_projects
    ,count_notifications=count_notifications,pagination=pagination,form=form,info=info,error=error)



@client_blueprint.route('/edit_group/<group_id>', methods=['GET', 'POST'])
@login_required
def edit_group(group_id):

    info = None
    error = None

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    form = CreateNewGroup(request.form)

    group = client_dao.get_group(group_id)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST' :
        result = client_dao.edit_group(group_id,form.name.data)
        if(result):
            error = 'Este grupo ya esta creado'
        else:
            info = 'Se actualizaron los datos del grupo'

    return render_template('views/group/edit_group.html',form=form,info=info,error=error,group=group
    ,count_projects=count_projects,count_notifications=count_notifications)


@client_blueprint.route('/new_assignment/', methods=['GET', 'POST'])
@login_required
def new_assignment():

    info = None
    error = None

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    form = NewAssignment(request.form)
    groups = client_dao.get_groups()
    count_groups = client_dao.get_groups_count()
    projects = project_dao.get_projects()
    count_projects = project_dao.get_projects_count()




    if request.method == 'POST' and form.validate_on_submit():
        result = client_dao.new_assign_client(form.group.data,form.project.data)
        if (result):
            error = 'El proyecto ya cuenta con el grupo asignado'
        else:
            info = 'Se ha asignado correctamente'
            return redirect(url_for('client.assignments',info=info,error=error))


    return render_template('views/client/new_assignment.html',info=info,error=error,form=form
    ,count_projects=count_projects,count_notifications=count_notifications,count_groups=count_groups
    ,groups=groups,projects=projects,count_project=count_projects)


@client_blueprint.route('/assignments', methods=['GET', 'POST'])
@login_required
def assignments():

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    form = NewAssignment(request.form)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    info = request.args.get('info',None)
    error = request.args.get('error',None)

    if request.method == 'POST':
        try:
            group = form.group.data
            project = form.project.data
            href = url_for("client.assignments", group=group,project=project)
            href += "&page={0}"
        except Exception as e:
            print(e)
    else:
        group = request.args.get("group", '')
        project = request.args.get("project", '')
        href = url_for("client.assignments", group=group,project=project)
        href += "&page={0}"

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    assignments = client_dao.get_assignments(group,project,offset,items_for_page)
    count_assignments = client_dao.get_assignments_count(group,project)

    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_assignments,
                            per_page_parameter=items_for_page,record_name='assignments',href=href)

    groups = client_dao.get_groups()
    count_groups = client_dao.get_groups_count()
    projects = project_dao.get_projects()
    count_projects = project_dao.get_projects_count()


    return render_template('views/client/assignments.html',count_projects=count_projects
    ,count_notifications=count_notifications,form=form,assignments=assignments,pagination=pagination
    ,groups=groups,count_groups=count_groups,projects=projects,projects_count=count_projects,info=info,error=error)


@client_blueprint.route('/edit_assignment/<assignment_id>/', methods=['GET', 'POST'])
@login_required
def edit_assignment(assignment_id):

    if session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    info = None
    error = None
    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    form = NewAssignment(request.form)

    groups = client_dao.get_groups_unfiltered()
    projects = project_dao.get_projects_unfiltered()

    assignment = client_dao.get_assignment(assignment_id)
    app.logger.debug('data %s',assignment)

    if request.method == 'POST' :

        project = project_dao.get_project(form.project.data)

        if (project['activo']):
            result = client_dao.update_assign_client(assignment_id,form.group.data,form.project.data)
            if (result):
                error = 'El proyecto ya cuenta con el grupo asignado'
            else:
                info = 'Se ha asignado correctamente'
        else:
            error = 'El proyecto seleccionado se encuentra inabilitado'



    return render_template('views/client/edit_assignment.html',count_projects=count_projects
    ,count_notifications=count_notifications,groups=groups,projects=projects,form=form
    ,assignment=assignment,info=info,error=error)

@client_blueprint.route('/module/<id_module>/', methods=['GET', 'POST'])
@login_required
def module(id_module):
    info = request.args.get('info',None)
    error = request.args.get('error',None)
    module = project_dao.get_sprint(id_module)

    if module['flag_cliente'] == False :
        return redirect(url_for('client.homec'))

    tasks = project_dao.get_tasks_sprint(id_module)
    tasks_count = project_dao.get_tasks_sprint_count(id_module)
    project = project_dao.get_project(module['id_proyecto'])
    user = user_dao.get_user(project['admin'])
    comments = project_dao.get_comments_module(id_module)
    documents = project_dao.get_documents_module(id_module)

    return render_template('views/client/module.html',admin=user,module=module,tasks=tasks,tasks_count=tasks_count
    ,comments=comments,documents=documents,id_module=id_module,info=info,error=error)



@client_blueprint.route('/new_document_client/<id_module>', methods=['GET', 'POST'])
@login_required
def new_document_client(id_module):
    info = None
    error = None

    form = NewDocumentModule(request.form)

    if request.method == 'POST'  :
        if 'document' not in request.files:
            error = 'Archivo no seleccionado'
            return render_template('views/client/new_document_client.html',form=form,info=info,error=error,id_module=id_module)

        file = request.files['document']
        if file.filename == '':
            error ='El arhivo no existe'

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            result = project_dao.add_docuemnt_module(id_module,filename)
            if (result):
                error = 'El documento ya se encuentra en los registros'
            else:
                file.save(os.path.join(app.config['UPLOAD_FOLDER_DOCUMENTS'], filename))
                info = 'Se ha subido el documento {}'.format(filename)
                return redirect(url_for('client.module',id_module=id_module,info=info))

    return render_template('views/client/new_document_client.html',form=form,info=info,error=error,id_module=id_module)


@client_blueprint.route('/validate_client_exist', methods=['GET', 'POST'])
@login_required
def validate_client_exist():

    user =  request.form['user']
    user_validate = client_dao.validate_client_exist(user)

    return "{}".format(user_validate)

@client_blueprint.route('/delete_client', methods=['GET', 'POST'])
@login_required
def delete_client():

    id = request.form['id']
    client_dao.delete_client(id)

    return ""

@client_blueprint.route('/delete_company', methods=['GET', 'POST'])
@login_required
def delete_company():

    id = request.form['id']
    client_dao.delete_company(id)

    return ""

@client_blueprint.route('/delete_group', methods=['GET', 'POST'])
@login_required
def delete_group():

    id = request.form['id']
    client_dao.delete_group(id)

    return ""

@client_blueprint.route('/delete_assignment', methods=['GET', 'POST'])
@login_required
def delete_assignment():

    id = request.form['id']
    client_dao.delete_assignment(id)

    return ""

@client_blueprint.route('/active_ticket', methods=['GET', 'POST'])
@login_required
def active_ticket():
    active = None
    id = request.form['id']
    new_active = request.form['active']
    if (new_active == 'true'):
        active = True
    else:
        active = False
    client_dao.update_active_ticket(id,active)
    return ""

@client_blueprint.route('/match_user_module', methods=['GET', 'POST'])
@login_required
def match_user_module():
    result = 'no_permitido'
    id = request.form['id']
    id_project = request.form['id_project']

    if session['rol'] == 'gerente' :
        result = 'gerente'
    else:
        result_match = client_dao.match_user_module(id,session['id_user'])
        if (result_match):
            result = 'permitido'
        else:
            result_admin = project_dao.match_admin_project(id_project,session['id'])
            if (result_admin):
                result = 'permitido'
    return result

@client_blueprint.route('/download_document/<id_document>', methods=['GET', 'POST'])
@login_required
def download_document(id_document):

    document = project_dao.get_document_module(id_document)
    return send_file(app.config['UPLOAD_FOLDER_DOCUMENTS']+'/'+document['clave'], attachment_filename=''+document['clave'],as_attachment=True)
