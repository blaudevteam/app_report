# coding=utf-8
from flask_wtf import FlaskForm
from wtforms import StringField,SelectField,DateField,IntegerField,FileField
from wtforms.validators import DataRequired


class ProjectFilterForm(FlaskForm):
    name = StringField('Nombre', validators=[])
    state = SelectField('Estado:',choices=[('', 'Todos'),('Pendiente', 'Pendientes'),('En desarrollo', 'En desarrollo'),('Finalizado', 'Finalizados')])

class CreateNewProject(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(message='Debe ingresar el nombre')])
    description = StringField('Descripcion', validators=[])

class EditProjectForm(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(message='Debe ingresar el nombre')])
    description = StringField('Descripcion', validators=[])


class AddSprintForm(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(message='Debe ingresar el nombre')])
    date = DateField('Fecha Estimada',format='%Y-%m-%d' ,validators=[DataRequired(message='Debe completar el formato correcto')])
    time = StringField('Horas Estimadas', validators=[DataRequired(message='Debe ingresar las horas')])
    journeys = StringField('Jornadas', validators=[DataRequired(message='Debe ingresar las jornadas')])
    state = SelectField('Estado:',choices=[('Pendiente', 'Pendientes'),('En proceso', 'En proceso')])
    sprint = StringField('Sprint', validators=[DataRequired(message='Debe ingresar el sprint')])
    priority = SelectField('Estado:',choices=[('baja', 'Baja'),('media', 'Media'),('alta', 'Alta')])
    priority2 = SelectField('Estado:',choices=[('media', 'Media'),('baja', 'Baja'),('alta', 'Alta')])
    priority3 = SelectField('Estado:',choices=[('alta', 'Alta'),('baja', 'Baja'),('media', 'Media')])
    description = StringField('Descripcion', validators=[])
    flag_journeys = StringField('Flag Jornadas', validators=[])



class AddTaskForm(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(message='Debe ingresar el nombre')])
    time = StringField('Horas Estimadas', validators=[DataRequired(message='Debe ingresar las horas')])
    coment = StringField('Comentario', validators=[])

class NewSprintProject(FlaskForm):
    name = StringField('Nombre', validators=[DataRequired(message='Debe ingresar el nombre')])

class NewDocumentModule(FlaskForm):
    document = FileField('Documento*', validators=[DataRequired(message='Debe ingresar el documento')])
