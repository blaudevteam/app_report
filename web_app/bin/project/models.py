import random
import pandas as pd
from web_app import bcrypt,app
from datetime import date
from datetime import datetime
from bson import ObjectId
from flask import session
from web_app.bin.user.models import UserDao
from web_app.bin.utils.utils import convert_file_byte_to_workbook,convert_file_to_bytes

NAME_COLUMNS_MODULES = {'Nombre':'nombre','Fecha estimada':'fechas_estimada','Sprint':'sprint','Prioridad':'prioridad'
                       ,'Descripcion':'descripcion','Horas/Jornadas':'flag_jornadas','Tiempo':'tiempo'}

DROP_COLUMNS_SPRINT = ['proyecto','activo']

RENAME_COLUMNS_SPRINT = {'nombre':'sprint'}

DROP_COLUMNS_MODULE = ['sprint']

RENAME_COLUMNS_MODULE = {'_id':'sprint'}

ORDER_COLUMNS_MODULE = ['nombre','estado','fecha_estimada','fecha_fin','horas_estimadas','id_proyecto','horas_fin'
                        ,'sprint','prioridad','descripcion','flag_cliente','usuario','tipo_explotacion','jornada','cliente'
                        ,'flag_jornadas']

class ProjectDao:

    def __init__(self, conn):
        self.db_project = conn
        self.user_dao = UserDao(conn)

    def get_projects(self,user_id=None,name=None,state=None,offset=0,items_for_page=0):
        query = self.__build_query(name,state,user_id)
        projects_db = self.db_project.db.proyectos.find(query).skip(offset).limit(items_for_page)
        projects =[{
            "id": str(file_rec['_id']),
            "nombre": file_rec['nombre'],
            "fecha_creacion": file_rec['fecha_creacion'].strftime("%d-%m-%Y %H:%M"),
            "fecha_fin":file_rec['fecha_fin'].strftime("%d-%m-%Y %H:%M"),
            "estado":file_rec['estado'],
            "horas_estimadas":file_rec['horas_estimadas'],
            "descripcion":file_rec['descripcion'],
            "admin":file_rec['admin']
        } for file_rec in projects_db]

        count_projects = self.get_projects_count(name,state)
        df_projects = pd.DataFrame()
        if(count_projects > 0):
            df_projects = pd.DataFrame(projects)
            df_projects['costo_estimado'] = df_projects.apply(lambda x: self.__calculate_estimated_cost(x['id']), axis=1)
            df_projects['horas_fin'] = df_projects.apply(lambda x: self.__count_project_task(x['id']), axis=1)
            df_projects['costo_real'] = df_projects.apply(lambda x: self.__calculate_real_cost(x['id']), axis=1)
            df_projects['eficiencia'] = df_projects.apply(lambda x: self.__get_project_efficiency(x), axis=1)

        return df_projects.to_dict('records')

    def get_projects_unfiltered(self):
        projects_db = self.db_project.db.proyectos.find({})
        projects =[{
            "id": str(file_rec['_id']),
            "nombre": file_rec['nombre'],
            "activo": file_rec['activo']
        } for file_rec in projects_db]

        return projects

    def get_projects_count(self,user_id=None,name=None,state=None):
        query = self.__build_query(name,state,user_id)
        return self.db_project.db.proyectos.find(query).count()


    def add_project(self,id_admin=None,name=None,time=None,description=None):
        project = self.db_project.db.proyectos.find_one({"nombre": name,"activo":True})

        if project:
            return False
        else:
            user = self.user_dao.get_user_by_username(id_admin)
            creation = datetime.today()
            self.db_project.db.proyectos.insert({"nombre": name
            ,"fecha_creacion":creation
            ,"fecha_fin":creation
            ,"estado":"Pendiente"
            ,"horas_estimadas":time
            ,"horas_fin":"0"
            ,"descripcion":description
            ,"activo":True
            ,"finalizado": False
            ,"costo_estimado": "0"
            ,"costo_real": "0"
            ,"admin":str(user['id'])
            })
            return True


    def get_project(self, id):
        project_db = self.db_project.db.proyectos.find_one({"_id": ObjectId(id)})
        if project_db:
            project = {"id": project_db['_id']
            ,"nombre":project_db['nombre']
            ,"estado":project_db['estado']
            ,"descripcion":project_db['descripcion']
            ,"fecha_creacion":project_db['fecha_creacion'].strftime("%d-%m-%Y %H:%M")
            ,"admin":project_db['admin']
            ,"horas_estimadas":project_db['horas_estimadas']
            ,"horas_invertidas":project_db['horas_fin']
            ,"activo":project_db['activo']
            }


            return project
        else:
            app.logger.debug('El proyecto no existe')

    def update_project(self,id=None,name=None,description=None):
        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "nombre": name,
                "descripcion": description
            }})

    def delete_project(self,id):
        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "activo": False
            }})

        sprint_db = self.db_project.db.sprints.find({"id_proyecto":id,"activo":True})
        sprint_db_count = self.db_project.db.sprints.find({"id_proyecto":id,"activo":True}).count()

        task_db = self.db_project.db.tareas.find({"id_proyecto":id,"activo":True})
        task_db_count = self.db_project.db.tareas.find({"id_proyecto":id,"activo":True}).count()

        member_db = self.db_project.db.miembros.find({"id_proyecto":id,"activo":True})
        member_db_count = self.db_project.db.miembros.find({"id_proyecto":id,"activo":True}).count()

        developer_db = self.db_project.db.developers_asignados.find({"id_proyecto":id,"activo":True})
        developer_db_count = self.db_project.db.developers_asignados.find({"id_proyecto":id,"activo":True}).count()

        assignation_db = self.db_project.db.asignaciones.find({"proyecto":id,"activo":True})
        assignations_db_count = self.db_project.db.asignaciones.find({"proyecto":id,"activo":True}).count()

        sprint_project_db = self.db_project.db.sprints_proyecto.find({"proyecto":id,"activo":True})
        sprint_project_db_count = self.db_project.db.sprints_proyecto.find({"proyecto":id,"activo":True}).count()

        record_db = self.db_project.db.historialProyecto.find({"id_proyecto":id,"activo":True})
        record_db_count = self.db_project.db.historialProyecto.find({"id_proyecto":id,"activo":True}).count()


        sprints =[{
            "id": str(file_rec['_id'])
        } for file_rec in sprint_db]
        tasks =[{
            "id": str(file_rec['_id'])
        } for file_rec in task_db]
        members =[{
            "id": str(file_rec['_id'])
        } for file_rec in member_db]
        developers =[{
            "id": str(file_rec['_id'])
        } for file_rec in developer_db]

        assignations =[{
            "id": str(file_rec['_id'])
        } for file_rec in assignation_db]

        sprints_project =[{
            "id": str(file_rec['_id'])
        } for file_rec in sprint_project_db]

        records =[{
            "id": str(file_rec['_id'])
        } for file_rec in record_db]

        if (sprint_db_count > 0):
            df_sprints = pd.DataFrame(sprints)
            df_sprints['delete_sprint'] = df_sprints.apply(lambda x: self.__delete_sprint_project(x), axis=1)
        if (task_db_count > 0):
            df_tasks = pd.DataFrame(tasks)
            df_tasks['delete_task'] = df_tasks.apply(lambda x: self.__delete_tasks_project(x), axis=1)
        if (member_db_count > 0):
            df_members = pd.DataFrame(members)
            df_members['delete_member'] = df_members.apply(lambda x: self.__delete_member_project(x), axis=1)
        if (developer_db_count > 0):
            df_developers = pd.DataFrame(developers)
            df_developers['delete_developer'] = df_developers.apply(lambda x: self.__delete_developer_project(x), axis=1)
        if (assignations_db_count > 0):
            df_assignations = pd.DataFrame(assignations)
            df_assignations['delete_assignations'] = df_assignations.apply(lambda x: self.__delete_assignatios_project(x), axis=1)
        if (sprint_project_db_count > 0):
            df_sprints_project = pd.DataFrame(sprints_project)
            df_sprints_project['delete_sprint_project'] = df_sprints_project.apply(lambda x: self.__delete_sprints_project(x), axis=1)
        if (record_db_count > 0):
            df_records = pd.DataFrame(records)
            df_records['delete_records'] = df_records.apply(lambda x: self.__delete_records_project(x), axis=1)


    def finalize_project(self,id):
        finalize = datetime.today()
        time_finalize = self.__count_project_task(id)
        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "estado":"Finalizado",
                "fecha_fin": finalize,
                "horas_fin":time_finalize,
                "finalizado":True
            }})

    def add_member(self,user_name,id_project):
        app.logger.debug("el nombre recibido es %s",user_name)
        user = self.user_dao.get_user_by_username(user_name)
        member = self.db_project.db.miembros.find_one({"id_proyecto": id_project, "id_usuario":str(user['id']), "activo":True })
        project = self.db_project.db.proyectos.find_one({"_id": ObjectId(id_project),"admin":str(user['id'])})
        if(member or project):
            return False
        else:
            self.db_project.db.miembros.insert({
            "id_proyecto": id_project
            ,"id_usuario":str(user['id'])
            ,"activo":True
            })
            return True

    def match_member_project(self,id_project,user_name):
        user = self.user_dao.get_user_by_username(user_name)
        member = self.db_project.db.miembros.find_one({"id_proyecto": id_project, "id_usuario":str(user['id']), "activo":True })

        if (member):
            return True
        else:
            return False

    def match_admin_project(self,id_project,user_name):
        user = self.user_dao.get_user_by_username(user_name)
        project = self.db_project.db.proyectos.find_one({"_id": ObjectId(id_project), "admin":str(user['id'])})
        if(project):
            return True
        else:
            return False

    def add_sprint(self,id,name,state,date,time,sprint,priority,description,is_client,user,exploitation,working_hours,client,flag_journeys):
        new_date = datetime.combine(date, datetime.min.time())
        creation = datetime.today()
        self.db_project.db.sprints.insert({
        "nombre": name
        ,"estado":state
        ,"fecha_estimada":new_date
        ,"fecha_fin":creation
        ,"horas_estimadas":time
        ,"id_proyecto":id
        ,"horas_fin":"0"
        ,"sprint":sprint
        ,"prioridad":priority
        ,"descripcion":description
        ,"flag_cliente":is_client
        ,"usuario":user
        ,"tipo_explotacion":exploitation
        ,"jornada":working_hours
        ,"cliente":client
        ,"flag_jornadas":flag_journeys
        ,"activo":True
        })


    def get_sprints(self,id):
        task_db = self.db_project.db.sprints.find({"id_proyecto":id,"activo":True})
        tasks =[{
            "id": str(file_rec['_id']),
            "nombre": file_rec['nombre'],
            "descripcion": file_rec['descripcion'],
            "estado": file_rec['estado'],
            "fecha_estimada": file_rec['fecha_estimada'].strftime("%d-%m-%Y"),
            "fecha_fin": file_rec['fecha_fin'].strftime("%d-%m-%Y"),
            "horas_estimadas": file_rec['horas_estimadas'],
            "id_proyecto": file_rec['id_proyecto'],
            "horas_fin": file_rec['horas_fin'],
            "flag_cliente": file_rec['flag_cliente'],
            "jornada": file_rec['jornada'],
            "flag_jornada": file_rec['flag_jornadas'],
            "prioridad": file_rec['prioridad'],
            "sprint": file_rec['sprint'],
            "activo": file_rec['activo']

        } for file_rec in task_db]

        df_module = pd.DataFrame(tasks)
        count_sprints = self.get_sprints_count(id)

        if (count_sprints > 0):
            df_module['sprint'] = df_module.apply(lambda x: self.__get_name_sprint(x),axis=1)
            # app.logger.debug(df_module['sprint'])

        return df_module.to_dict('records')

    def get_sprint(self,id):
        task_db = self.db_project.db.sprints.find_one({"_id": ObjectId(id)})
        if task_db:
            task = {"id": task_db['_id']
            ,"nombre":task_db['nombre']
            ,"estado":task_db['estado']
            ,"fecha_estimada":task_db['fecha_estimada'].strftime("%d-%m-%Y")
            ,"fecha_fin":task_db['fecha_fin'].strftime("%d-%m-%Y %H:%M")
            ,"horas_estimadas":task_db['horas_estimadas']
            ,"id_proyecto":task_db['id_proyecto']
            ,"horas_fin":task_db['horas_fin']
            ,"sprint":task_db['sprint']
            ,"jornada":task_db['jornada']
            ,"descripcion":task_db['descripcion']
            ,"prioridad":task_db['prioridad']
            ,"flag_cliente":task_db['flag_cliente']
            ,"flag_jornada":task_db['flag_jornadas']
            ,"activo":task_db['activo']
            }
        return task

    def get_sprints_pendiente(self,id):
        return self.db_project.db.sprints.find({"id_proyecto":id,"estado":"Pendiente","activo":True}).count()
    def get_sprints_desarrollo(self,id):
        return self.db_project.db.sprints.find({"id_proyecto":id,"estado":"En proceso","activo":True}).count()
    def get_sprints_final(self,id):
        return self.db_project.db.sprints.find({"id_proyecto":id,"estado":"Finalizado","activo":True}).count()


    def get_sprints_count(self,id):
        return self.db_project.db.sprints.find({"id_proyecto":id,"activo":True}).count()

    def update_sprint_state(self,id,state,hours):
        self.db_project.db.sprints.update_one({"_id": ObjectId(id)}, {"$set": {"estado":state}})


    def delete_task(self,id,id_project):
        self.db_project.db.tareas.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "activo":False
            }})
        sum = self.__count_project_task(id_project)
        cost = self.__calculate_real_cost(id_project)


    def get_count_members(self,id):
        return self.db_project.db.miembros.find({"id_proyecto":id,"activo":True}).count()

    def get_records(self,id):
        records_db = self.db_project.db.historialProyecto.find({"id_proyecto":id,"activo":True}).sort('fecha_creacion', -1)
        records =[{
            "id": str(file_rec['_id'])
            ,"fecha_creacion": file_rec['fecha_creacion'].strftime("%d-%m-%Y %H:%M")
            ,"nombre": file_rec['nombre']
            ,"descripcion": file_rec['descripcion']
            ,"position": file_rec['position']
            ,"color": file_rec['color']
            ,"id_proyecto": file_rec['id_proyecto']
            ,"id_usuario": file_rec['id_usuario']
            ,"activo": file_rec['activo']
        } for file_rec in records_db]
        return records


    def new_record(self,id,user_name,name,message):

        user = self.user_dao.get_user_by_username(user_name)

        position = random.choice( ['right', 'left'] )
        color = random.choice( ['primary', 'darkorange'] )

        creation = datetime.today()
        self.db_project.db.historialProyecto.insert({
        "fecha_creacion": creation
        ,"nombre": name
        ,"descripcion": message
        ,"position": position
        ,"color": color
        ,"id_proyecto": id
        ,"id_usuario": str(user['id'])
        ,"activo":True
        })


    def get_projects_user_count(self,user_name):

        count_projects = 0

        user = self.user_dao.get_user_by_username(user_name)

        if (session['rol']=='gerente'):
            projects = self.db_project.db.proyectos.find({"activo":True}).count()
        else:
            projects = self.db_project.db.proyectos.find({"activo":True,"admin":str(user['id'])}).count()
            members = self.db_project.db.miembros.find({"id_usuario":str(user['id']),"activo":True}).count()
            count_projects += members


        count_projects += projects


        return count_projects

    def get_members(self,id_project):
        records_db = self.db_project.db.miembros.find({"id_proyecto":id_project,"activo":True})
        records_db_count = self.db_project.db.miembros.find({"id_proyecto":id_project,"activo":True}).count()
        records =[{
            "id": str(file_rec['_id'])
            ,"id_proyecto": file_rec['id_proyecto']
            ,"id_usuario": file_rec['id_usuario']
            ,"activo": file_rec['activo']
        } for file_rec in records_db]

        df_records = pd.DataFrame(records)
        if (records_db_count > 0):
            df_records['user_name'] = df_records.apply(lambda x: self.__get_users_name(x), axis=1)

        return df_records.to_dict('records')

    def remove_member(self,id):
        self.db_project.db.miembros.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "activo":False
            }})

    def project_to_development(self,id_project):
        self.db_project.db.proyectos.update_one({"_id": ObjectId(id_project)}, {
            "$set": {
                "estado": "En desarrollo",
                "finalizado":False
            }})

    def update_project_to_development(self,id_project):
        to_day = datetime.today()
        self.db_project.db.proyectos.update_one({"_id": ObjectId(id_project)}, {
            "$set": {
                "estado": "En desarrollo",
                "fecha_creacion": to_day,
                "finalizado":False
            }})



    def get_my_projects(self,id_user):
        if(session['rol']=='gerente'):
            projects_db = self.db_project.db.proyectos.find({"activo":True,"finalizado":False})
            projects_db_count = self.db_project.db.proyectos.find({"activo":True,"finalizado":False}).count()
        else:
            projects_db = self.db_project.db.proyectos.find({"admin":id_user,"activo":True,"finalizado":False})
            projects_db_count = self.db_project.db.proyectos.find({"admin":id_user,"activo":True,"finalizado":False}).count()

            miembros_db = self.db_project.db.miembros.find({"id_usuario":id_user,"activo":True})
            miembros_db_count = self.db_project.db.miembros.find({"id_usuario":id_user,"activo":True}).count()

            miembros = [{
            "id": file_rec['id_proyecto']
            } for file_rec in miembros_db]

            df_members = pd.DataFrame(miembros)

            if (miembros_db_count > 0):
                df_members['nombre'] = df_members.apply(lambda x: self.__get_projects_name(x), axis=1)
                df_members['finalizadas'] = df_members.apply(lambda x: self.__get_count_sprint_finished(x),axis=1)
                df_members['pendientes'] = df_members.apply(lambda x: self.__get_count_sprint_pending(x),axis=1)
                df_members['desarrollo'] = df_members.apply(lambda x: self.__get_count_sprint_develop(x),axis=1)
                df_members['total'] = df_members.apply(lambda x: self.__get_count_sprint_total(x),axis=1)
                df_members['porcentaje'] = df_members.apply(lambda x: self.__get_finish_percent(x),axis=1)
                df_members['color'] = df_members.apply(lambda x: self.__get_rand_color(x),axis=1)
                df_members['horas'] = df_members.apply(lambda x: self.__get_hours_actual(x),axis=1)
                df_members['horas_estimadas'] = df_members.apply(lambda x: self.__get_hours_estimated(x),axis=1)
                df_members['cost'] = df_members.apply(lambda x: self.__update_cost_project(x),axis=1)

        projects =[{
            "id": str(file_rec['_id']),
            "nombre": file_rec['nombre'],
            "horas_estimadas": file_rec['horas_estimadas']
        } for file_rec in projects_db]


        df_projects = pd.DataFrame(projects)


        if (projects_db_count > 0):
            df_projects['finalizadas'] = df_projects.apply(lambda x: self.__get_count_sprint_finished(x),axis=1)
            df_projects['pendientes'] = df_projects.apply(lambda x: self.__get_count_sprint_pending(x),axis=1)
            df_projects['desarrollo'] = df_projects.apply(lambda x: self.__get_count_sprint_develop(x),axis=1)
            df_projects['total'] = df_projects.apply(lambda x: self.__get_count_sprint_total(x),axis=1)
            df_projects['porcentaje'] = df_projects.apply(lambda x: self.__get_finish_percent(x),axis=1)
            df_projects['color'] = df_projects.apply(lambda x: self.__get_rand_color(x),axis=1)
            df_projects['horas'] = df_projects.apply(lambda x: self.__get_hours_actual(x),axis=1)
            df_projects['cost'] = df_projects.apply(lambda x: self.__update_cost_project(x),axis=1)

        if(session['rol']=='gerente'):
            result = df_projects
        else:
            result = df_projects.append(df_members)

        return result.to_dict('records')


    def get_detail_sprint(self,id_sprint):
        sprint_db = self.db_project.db.sprints.find_one({"_id": ObjectId(id_sprint)})
        if sprint_db:
            sprint = {"id": str(sprint_db['_id'])
            ,"nombre":sprint_db['nombre']
            ,"horas_estimadas":sprint_db['horas_estimadas']
            ,"estado":sprint_db['estado']
            ,"flag_cliente":sprint_db['flag_cliente']
            ,"jornada":sprint_db['jornada']
            ,"fecha_estimada":sprint_db['fecha_estimada'].strftime("%d-%m-%Y")
            }
            return sprint

    def get_tasks_sprint(self,id_sprint):
        task_db = self.db_project.db.tareas.find({"id_sprint":id_sprint,"activo":True})
        task_db_count = self.db_project.db.tareas.find({"id_sprint":id_sprint,"activo":True}).count()
        tasks =[{
            "id": str(file_rec['_id'])
            ,"id_usuario": file_rec['id_usuario']
            ,"nombre": file_rec['nombre']
            ,"comentario": file_rec['comentario']
            ,"fecha_creacion": file_rec['fecha_creacion'].strftime("%d-%m-%Y")
            ,"horas_invertidas": file_rec['horas_invertidas']
        } for file_rec in task_db]

        df_tasks = pd.DataFrame(tasks)

        if (task_db_count > 0):
            df_tasks['nombre_usuario'] = df_tasks.apply(lambda x: self.__get_users_name(x),axis=1)


        return df_tasks.to_dict('records')

    def get_tasks_sprint_count(self,id_sprint):
        return self.db_project.db.tareas.find({"id_sprint":id_sprint,"activo":True}).count()

    def add_task(self,id_project,id_user,id,name,time,coment):
        creation = datetime.today()
        self.db_project.db.tareas.insert({
        "id_usuario": id_user
        ,"id_proyecto": id_project
        ,"id_sprint": id
        ,"nombre": name
        ,"comentario": coment
        ,"horas_invertidas": time
        ,"fecha_creacion": creation
        ,"activo": True
        })

        sum = self.__count_project_task(id_project)
        cost = self.__calculate_real_cost(id_project)


    def get_task(self,id):
        task_db = self.db_project.db.tareas.find_one({"_id": ObjectId(id)})
        if task_db:
            task = {"id": task_db['_id']
            ,"nombre":task_db['nombre']
            }
            return task

    def validate_sprint_to_finished(self,id):
        user_db = self.db_project.db.tareas.find_one({"id_sprint":id,"activo":True})
        if user_db:
            return True
        else:
            return False

    def delete_sprint(self,id_sprint,id_project):
        self.db_project.db.sprints.update_one({"_id": ObjectId(id_sprint)}, {
            "$set": {
                "activo":False
            }})

        tasks = self.get_tasks_sprint(id_sprint)
        tasks_count = self.get_tasks_sprint_count(id_sprint)
        if (tasks_count > 0):
            df_tasks = pd.DataFrame(tasks)
            df_tasks['test'] = df_tasks.apply(lambda x: self.__delete_tasks_sprint(x), axis=1)

        match_develops =self.get_developers_sprint(id_sprint)
        match_develops_count =self.get_developers_sprint_count(id_sprint)
        if (match_develops_count > 0):
            df_develops = pd.DataFrame(match_develops)
            df_develops['test'] = df_develops.apply(lambda x: self.__delete_develop_sprint(x), axis=1)

        sum = self.__count_project_task(id_project)
        cost = self.__calculate_real_cost(id_project)
        result = self.__calculate_estimated_cost(id_project)

    def match_developer_sprint(self,id,id_sprint):
        developer = self.db_project.db.developers_asignados.find_one({"id_sprint": id_sprint, "id_usuario":id, "activo":True })
        if (developer):
            return True
        else:
            return False


    def add_developer_to_project(self,id_sprint,user_name,id_project):
        validation = False
        info = ""
        user = self.user_dao.get_user_by_username(user_name)
        developer = self.db_project.db.developers_asignados.find_one({"id_sprint": id_sprint, "id_usuario":str(user['id']), "activo":True })

        if (developer):
            info = "asignado"
            validation = False
        else:
            result_member = self.match_member_project(id_project,user_name)
            if(result_member):
                if (str(user['rol']) =="developer" or str(user['rol'])=="gestor"):
                    info = "asignar"
                    validation = True
                else:
                    info = "rol"
                    validation = False
            else:
                info = "miembro"
                validation = False


        if (validation):
            self.db_project.db.developers_asignados.insert({
            "id_sprint": id_sprint
            ,"id_usuario":str(user['id'])
            ,"id_proyecto":id_project
            ,"activo":True
            })

            result = self.__calculate_estimated_cost(id_project)

        return info

    def get_developers_sprint(self,id_sprint):
        match_develop_db = self.db_project.db.developers_asignados.find({"id_sprint":id_sprint,"activo":True})
        match_develop_db_count = self.db_project.db.developers_asignados.find({"id_sprint":id_sprint,"activo":True}).count()
        match_develops =[{
            "id": str(file_rec['_id']),
            "id_usuario": file_rec['id_usuario'],
            "id_sprint": file_rec['id_sprint']
        } for file_rec in match_develop_db]

        df_develops = pd.DataFrame(match_develops)
        if (match_develop_db_count > 0):
            df_develops['usuario'] = df_develops.apply(lambda x: self.__get_users_name(x), axis=1)

        return df_develops.to_dict('records')

    def get_developers_sprint_count(self,id_sprint):
        return self.db_project.db.developers_asignados.find({"id_sprint":id_sprint,"activo":True}).count()


    def delete_develop_sprint(self,id_sprint,id_user,id_project):

        self.db_project.db.developers_asignados.update_one({"id_sprint":id_sprint,"id_usuario":id_user,"activo":True}, {
            "$set": {
                "activo":False
            }})


        result = self.__calculate_estimated_cost(id_project)


    def delete_projects_gestor(self,id_user):
        user = self.user_dao.get_user(id_user)
        if(user['rol'] == "gestor"):
            projects_count = self.db_project.db.proyectos.find({"admin":id_user,"activo":True}).count()
            if (projects_count > 0):
                projects_db = self.db_project.db.proyectos.find({"admin":id_user,"activo":True})
                projects = [{"id": str(file_rec['_id'])} for file_rec in projects_db]
                df_projects = pd.DataFrame(projects)
                df_projects['delete_project'] = df_projects.apply(lambda x: self.__delete_project(x), axis=1)

    def get_project_by_name(self,project_name):
        project_db = self.db_project.db.proyectos.find_one({"nombre": project_name,"activo":True})
        if project_db:
            project = {"id": project_db['_id']}
            return project

    def get_sprints_project_count(self,id):
        return self.db_project.db.sprints_proyecto.find({"proyecto":id,"activo":True}).count()

    def get_sprints_project_count_unfiltered(self,id):
        return self.db_project.db.sprints_proyecto.find({"proyecto":id}).count()

    def get_sprints_project(self,id):

        sprints_project_db = self.db_project.db.sprints_proyecto.find({"proyecto":id,"activo":True})
        sprints_project =[{
            "id": str(file_rec['_id'])
            ,"nombre":file_rec['nombre']
        } for file_rec in sprints_project_db]

        count_sprints = self.get_sprints_project_count(id)
        df_sprints = pd.DataFrame(sprints_project)
        df_sprints['modulos'] = 0
        if (count_sprints > 0):
            pass
            df_sprints['modulos'] = df_sprints.apply(lambda x: self.__get_count_modules(x), axis=1)

        return df_sprints.to_dict('records')

    def get_sprints_project_unfiltered(self,id):

        sprints_project_db = self.db_project.db.sprints_proyecto.find({"proyecto":id})
        sprints_project =[{
            "id": str(file_rec['_id'])
            ,"nombre":file_rec['nombre']
            ,"activo":file_rec['activo']
        } for file_rec in sprints_project_db]

        count_sprints = self.get_sprints_project_count_unfiltered(id)
        df_sprints = pd.DataFrame(sprints_project)
        df_sprints['modulos'] = 0
        if (count_sprints > 0):
            pass
            df_sprints['modulos'] = df_sprints.apply(lambda x: self.__get_count_modules(x), axis=1)

        return df_sprints.to_dict('records')

    def delete_sprint_project(self, id):
        self.db_project.db.sprints_proyecto.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "activo":False
            }})

    def add_sprint_project(self,id,name):
        sprint = self.db_project.db.sprints_proyecto.find_one({"proyecto": id,"nombre":name,"activo":True})

        if (sprint):
            return True
        else:
            self.db_project.db.sprints_proyecto.insert({
            "nombre": name,
            "proyecto":id
            ,"activo":True
            })
            return False

    def get_sprint_project(self,id):

        sprint_project_db = self.db_project.db.sprints_proyecto.find_one({"_id": ObjectId(id)})
        sprint_project = {"id": sprint_project_db['_id']
        ,"nombre":sprint_project_db['nombre']
        }

        return sprint_project

    def update_module(self,id,name,time,sprint,flag_client,journeys,priority,flag_journeys):
        self.db_project.db.sprints.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "nombre":name
                ,"horas_estimadas":time
                ,"jornada":journeys
                ,"flag_jornadas":flag_journeys
                ,"sprint":sprint
                ,"prioridad":priority
            }})

    def update_sprint_project(self,id,name):
        sprint = self.db_project.db.sprints_proyecto.find_one({"nombre": name,"activo":True})

        if (sprint):
            return True
        else:
            self.db_project.db.sprints_proyecto.update_one({"_id": ObjectId(id)}, {
                "$set": {
                    "nombre":name
                }})
            return False

    def get_comments_module(self,id):
        comments_db = self.db_project.db.comentarios_modulo.find({"modulo":id,"activo":True})
        count_comments_db = self.db_project.db.comentarios_modulo.find({"modulo":id,"activo":True}).count()
        comments =[{
            "id": str(file_rec['_id']),
            "usuario": file_rec['usuario'],
            "mensaje": file_rec['mensaje'],
            "editado": file_rec['editado'],
            "creacion": file_rec['creacion'].strftime("%d-%m-%Y %H:%M"),
        } for file_rec in comments_db]

        df_comments = pd.DataFrame(comments)
        df_comments['img_user'] = 'avatar.png'
        if (count_comments_db > 0):
             df_comments['img_user'] = df_comments.apply(lambda x: self.__get_img_user(x), axis=1)

        return df_comments.to_dict('records')

    def add_comment_module(self,id,message,user):
        creation = datetime.today()
        self.db_project.db.comentarios_modulo.insert({
        "modulo": id
        ,"usuario": user
        ,"creacion": creation
        ,"mensaje": message
        ,"editado":False
        ,"activo":True
        })

    def validate_new_comment_module(self,id):
        comments_db = self.db_project.db.comentarios_modulo.find({"modulo":id,"activo":True}).sort("_id", -1).limit(1)
        count_comments_db = self.db_project.db.comentarios_modulo.find({"modulo":id,"activo":True}).sort("_id", -1).limit(1).count()
        comments =[{
            "id": str(file_rec['_id']),
            "usuario": file_rec['usuario'],
            "mensaje": file_rec['mensaje'],
            "editado": file_rec['editado'],
            "creacion": file_rec['creacion'].strftime("%d-%m-%Y %H:%M"),
        } for file_rec in comments_db]

        df_comments = pd.DataFrame(comments)
        df_comments['img_user'] = 'avatar.png'
        if (count_comments_db > 0):
             df_comments['img_user'] = df_comments.apply(lambda x: self.__get_img_user(x), axis=1)

        return df_comments.to_dict('records')

    def match_user_comment(self,id_comment,id_user):
        match = self.db_project.db.comentarios_modulo.find_one({"_id": ObjectId(id_comment),"usuario":id_user,"activo":True})
        if (match):
            return True
        else:
            return False

    def delete_comment_module(self,id):
        self.db_project.db.comentarios_modulo.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "mensaje":'El comentario fue eliminado',
                "editado":True
            }})

    def update_time_project(self,id,flag_journeys,time,journeys,value_journey):
        new_hour_project = 0
        if (flag_journeys == "0"):
            new_hour_project = float(time)
        else:
            new_hour_project = float(journeys) * value_journey

        project = self.get_project(id)

        new_hour_project += float(project['horas_estimadas'])

        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "horas_estimadas":new_hour_project
            }})

    def delete_time_project(self,id,flag_journeys,time,journeys,value_journey):
        new_hour_project = 0
        if (flag_journeys == "0"):
            new_hour_project = time
        else:
            new_hour_project = float(journeys) * value_journey

        project = self.get_project(id)

        new_hour_project = float(project['horas_estimadas']) - new_hour_project

        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "horas_estimadas":new_hour_project
            }})

    def get_documents_module(self,id):
        documents_db = self.db_project.db.documentos_modulo.find({"modulo":id,"activo":True})
        documents =[{
            "id": str(file_rec['_id'])
            ,"nombre": file_rec['nombre']
            ,"clave": file_rec['clave']
            ,"extension": file_rec['extension']
            ,"activo": file_rec['activo']
        } for file_rec in documents_db]

        df_documents = pd.DataFrame(documents)
        df_documents['img'] = 'not_found.png'
        count_documents = self.get_count_documents_module(id)
        if(count_documents > 0):
            df_documents['img'] = df_documents.apply(lambda x: self.__get_img_document(x), axis=1)

        return df_documents.to_dict('records')

    def get_count_documents_module(self,id):
        return self.db_project.db.documentos_modulo.find({"modulo":id,"activo":True}).count()

    def add_docuemnt_module(self,id,filename):
        document = self.db_project.db.documentos_modulo.find_one({"clave": filename,"modulo":id,"activo":True})
        if (document):
            return True
        else:
            data_split = filename.split('.')
            creation = datetime.today()
            self.db_project.db.documentos_modulo.insert({
                "fecha_creacion": creation
                ,"modulo":id
                ,"nombre":data_split[0]
                ,"clave":filename
                ,"extension":data_split[1]
                ,"activo":True
            })
            return False

    def get_document_module(self,id):
        document_db = self.db_project.db.documentos_modulo.find_one({"_id": ObjectId(id)})
        if document_db:
            document = {
            "id": document_db['_id']
            ,"clave":document_db['clave']
            ,"activo":document_db['activo']
            }
        return document

    def upload_excel_modules(self,file,id_project,value_journey):
        file_bytes = convert_file_to_bytes(file)
        data_frame = self.convert_file_to_frame(file_bytes,id_project)
        data_frame['upload'] = data_frame.apply(lambda x: self.__upload_module(x,id_project,value_journey),axis=1)
        app.logger.debug(data_frame)
    def convert_file_to_frame(self, file_byte,id_project):
        data_frame_modules = pd.read_excel(convert_file_byte_to_workbook(file_byte), engine='xlrd')
        data_frame_modules = data_frame_modules.rename(columns = NAME_COLUMNS_MODULES)
        data_frame_modules['id_proyecto'] = id_project
        data_frame_modules['estado'] = 'Pendiente'
        data_frame_modules['horas_fin'] = '0'
        data_frame_modules['flag_cliente'] = False
        data_frame_modules['usuario'] = '---'
        data_frame_modules['tipo_explotacion'] = '---'
        data_frame_modules['cliente'] = '---'
        data_frame_modules['fecha_estimada'] = data_frame_modules.apply(lambda x: self.__convert_time_to_date(x),axis=1)
        data_frame_modules['fecha_fin'] = data_frame_modules.apply(lambda x: self.__convert_time_to_date(x),axis=1)
        data_frame_modules['flag_jornadas'] = data_frame_modules.apply(lambda x: self.__process_time(x),axis=1)
        data_frame_modules['jornada'] = data_frame_modules.apply(lambda x: self.__get_journey(x),axis=1)
        data_frame_modules['horas_estimadas'] = data_frame_modules.apply(lambda x: self.__get_hours_estimated_excel(x),axis=1)
        cursor = self.db_project.db.sprints_proyecto.find({'proyecto':id_project,'activo':True})
        df_sprints = pd.DataFrame(list(cursor))
        df_sprints = df_sprints.drop(columns = DROP_COLUMNS_SPRINT)
        df_sprints =df_sprints.rename(columns = RENAME_COLUMNS_SPRINT)
        data_frame_modules = pd.merge(data_frame_modules,df_sprints, on= 'sprint',how='left')
        data_frame_modules['_id'] = data_frame_modules.apply(lambda x: self.__show_type(x),axis=1)
        data_frame_modules = data_frame_modules.drop(columns = DROP_COLUMNS_MODULE)
        data_frame_modules = data_frame_modules.rename(columns = RENAME_COLUMNS_MODULE)
        data_frame_modules = data_frame_modules[ORDER_COLUMNS_MODULE]
        return data_frame_modules

    def __get_name_sprint(self,row):
        id_sprint = row['sprint']
        module_db = self.db_project.db.sprints_proyecto.find_one({"_id": ObjectId(row['sprint']),"activo":True})
        if module_db:
            sprint = {
            "nombre":module_db['nombre']
            }
            id_sprint = sprint['nombre']
        return id_sprint

    def __upload_module(self,data_frame,id_project,value_journey):
        self.db_project.db.sprints.insert({
            "nombre": data_frame['nombre']
            ,"estado":data_frame['estado']
            ,"fecha_estimada":data_frame['fecha_estimada']
            ,"fecha_fin":data_frame['fecha_fin']
            ,"horas_estimadas":data_frame['horas_estimadas']
            ,"id_proyecto":data_frame['id_proyecto']
            ,"horas_fin":data_frame['horas_fin']
            ,"sprint":data_frame['sprint']
            ,"prioridad":data_frame['prioridad']
            ,"descripcion":data_frame['descripcion']
            ,"flag_cliente":data_frame['flag_cliente']
            ,"usuario":data_frame['usuario']
            ,"tipo_explotacion":data_frame['tipo_explotacion']
            ,"jornada":data_frame['jornada']
            ,"cliente":data_frame['cliente']
            ,"flag_jornadas":data_frame['flag_jornadas']
            ,"activo":True
            })

        if (data_frame['flag_jornadas'] == False):
            flag_jornada = '0'
        else:
            flag_jornada = '1'

        self.update_time_project(id_project,flag_jornada,data_frame['horas_estimadas'],data_frame['jornada'],value_journey)
        return True

    def __show_type(self,row):
        return str(row['_id'])

    def __convert_time_to_date(self,row):
        test = datetime.strptime(row['fechas_estimada'], '%Y-%m-%d')
        return test

    def __process_time(self,row):
        if (row['flag_jornadas'] == 'Jornada'):
            return True
        else:
            return False

    def __get_journey(self,row):
        if (row['flag_jornadas'] == True):
            return row['tiempo']
        else:
            return 0

    def __get_hours_estimated_excel(self,row):
        if (row['flag_jornadas'] == False):
            return str(row['tiempo'])
        else:
            return '0'

    def __delete_project(self,row):
        self.delete_project(row['id'])
        return "---"

    def __delete_sprint_project(self,row):
        self.db_project.db.sprints.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"
    def __delete_tasks_project(self,row):
        self.db_project.db.tareas.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"
    def __delete_member_project(self,row):
        self.db_project.db.miembros.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"
    def __delete_developer_project(self,row):
        self.db_project.db.developers_asignados.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"
    def __delete_assignatios_project(self,row):
        self.db_project.db.asignaciones.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"

    def __delete_sprints_project(self,row):
        self.db_project.db.sprints_proyecto.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"

    def __delete_records_project(self,row):
        self.db_project.db.historialProyecto.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"


    def __update_cost_project(self,row):
        cost = self.__calculate_real_cost(str(row['id']))
        return cost

    def __delete_tasks_sprint(self,row):
        self.db_project.db.tareas.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"

    def __delete_develop_sprint(self,row):
        self.db_project.db.developers_asignados .update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"



    def __get_hours_actual(self,row):
        return self.__count_project_task(row['id'])
    def __get_count_sprint_finished(self,row):
        count_sprint_finished = self.get_sprints_final(row['id'])
        return count_sprint_finished
    def __get_count_sprint_pending(self,row):
        count_sprint_pending = self.get_sprints_pendiente(row['id'])
        return count_sprint_pending
    def __get_count_sprint_develop(self,row):
        count_sprint_develop = self.get_sprints_desarrollo(row['id'])
        return count_sprint_develop


    def __get_count_sprint_total(self,row):
        count_sprint_finished = self.get_sprints_final(row['id'])
        count_sprint_pending = self.get_sprints_pendiente(row['id'])
        count_sprint_develop = self.get_sprints_desarrollo(row['id'])

        result_count = count_sprint_finished + count_sprint_pending + count_sprint_develop
        return result_count

    def __get_finish_percent(self,row):
        total = row['total']
        finished = row['finalizadas']
        if (total > 0):
            result_percent = int((finished * 100) / total)
        else:
            result_percent = 0

        return result_percent

    def __get_rand_color(self,row):
        return random.choice( ['danger','warning','inverse','primary'] )

    def __get_users_name(self,row):
        user_db = self.db_project.db.usuarios.find_one({"_id": ObjectId(row['id_usuario']),"active":True})
        if user_db:
            user = {
            "nombre":user_db['user_name']
            }
        return user['nombre']

    def __get_projects_name(self,row):
        project_db = self.db_project.db.proyectos.find_one({"_id":ObjectId(row['id']),"activo":True})
        if project_db:
            project = {
            "nombre":project_db['nombre']
            }
        return project['nombre']

    def __get_hours_estimated(self,row):
        project_db = self.db_project.db.proyectos.find_one({"_id":ObjectId(row['id']),"activo":True})
        if project_db:
            project = {
            "horas_estimadas":project_db['horas_estimadas']
            }
        return project['horas_estimadas']


    def __build_query(self,name=None,state=None,user_id=None):
        query = {"activo":True}

        if name:
            query["nombre"] = {"$regex": name, "$options": 'i'}

        if state:
            query["estado"] = {"$regex": state, "$options": 'i'}

        if user_id:
            query['admin'] = user_id

        return query

    def __count_project_task(self,id):
        suma = 0
        task_db = self.db_project.db.tareas.find({"id_proyecto":id,"activo":True})
        tasks =[{
            "horas_invertidas": file_rec['horas_invertidas']
        } for file_rec in task_db]

        for task in tasks:
            suma += float(task['horas_invertidas'])

        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "horas_fin":str(suma)
            }})

        return suma

    def __calculate_real_cost(self,id):
        cost = 0
        task_db = self.db_project.db.tareas.find({"id_proyecto":id,"activo":True})
        task_db_count = self.db_project.db.tareas.find({"id_proyecto":id,"activo":True}).count()
        tasks =[{
            "horas_invertidas": file_rec['horas_invertidas']
            ,"id_usuario": file_rec['id_usuario']
        } for file_rec in task_db]

        df_tasks = pd.DataFrame(tasks)
        if (task_db_count > 0):
            df_tasks['user_cost'] = df_tasks.apply(lambda x: self.__get_users_cost(x), axis=1)
            df_tasks['cost_project'] = df_tasks.apply(lambda x: self.__get_cost_project(x), axis=1)
            cost = df_tasks['cost_project'].sum()
            self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
                "$set": {
                    "costo_real": str(cost)
                }})

        return cost

    def __get_users_cost(self,row):
        user_db = self.db_project.db.usuarios.find_one({"_id": ObjectId(row['id_usuario'])})
        if user_db:
            user = {
            "costo_hora":user_db['costo_hora']
            }
        return user['costo_hora']

    def __get_cost_project(self,row):
        cost = float(row['user_cost']) * float(row['horas_invertidas'])
        return cost


    def __calculate_estimated_cost(self,id):

        cost = 0
        developer_db = self.db_project.db.developers_asignados.find({"id_proyecto":id,"activo":True})
        developer_db_count = self.db_project.db.developers_asignados.find({"id_proyecto":id,"activo":True}).count()

        if (developer_db_count > 0):
            developers =[{
                "id_usuario": file_rec['id_usuario']
                ,"id_sprint": file_rec['id_sprint']
            } for file_rec in developer_db]

            df_developers = pd.DataFrame(developers)
            df_developers['horas_invertidas'] = df_developers.apply(lambda x: self.__get_time_sprint(x), axis=1)
            df_developers['user_cost'] = df_developers.apply(lambda x: self.__get_users_cost(x), axis=1)
            df_developers['cost_estimated'] = df_developers.apply(lambda x: self.__get_cost_project(x), axis=1)
            cost = df_developers['cost_estimated'].sum()
        self.db_project.db.proyectos.update_one({"_id": ObjectId(id)}, {
            "$set": {
                "costo_estimado": str(cost)
            }})
        return cost

    def __get_time_sprint(self,row):
        time = 0
        sprint_db = self.db_project.db.sprints.find_one({"_id": ObjectId(row['id_sprint']),"activo":True})
        if sprint_db:
            sprint = {
            "horas_estimadas":sprint_db['horas_estimadas']
            ,"jornada":sprint_db['jornada']
            ,"flag_jornada":sprint_db['flag_jornadas']
            }

            if (sprint['flag_jornada']):
                time = float(sprint['jornada']) * 8
            else:
                time = float(sprint['horas_estimadas'])
        return time

    def __get_project_efficiency(self,row):
        result = 0
        cost_estimated = float(row['costo_estimado'])
        cost_real = float(row['costo_real'])

        if (cost_real > 0):
            if (cost_estimated > 0):
                result = (cost_real * 100)/cost_estimated
                result = round(100 -result ,2)
        return result

    def __get_count_modules(self,row):
        return self.db_project.db.sprints.find({"sprint":str(row['id'])}).count()

    def __get_img_user(self,row):
        img='avatar2.png'
        user_db = self.db_project.db.usuarios.find_one({"_id": ObjectId(row['usuario'])})
        if user_db:
            user = {
            "img":user_db['img']
            }
            img = user['img']
        else:
            client_db = self.db_project.db.clientes.find_one({"_id": ObjectId(row['usuario'])})
            if (client_db):
                client = {
                "img":client_db['img']
                }
                img = client['img']
        return img

    def __get_img_document(self,row):
        img = 'not_found.png'
        if (row['extension'] == 'png' or row['extension'] == 'jpg' or row['extension'] == 'jpeg' or row['extension'] == 'PNG'):
            img = 'imagen.png'
        elif(row['extension'] == 'docx' or row['extension'] == 'docm' or row['extension'] == 'dotx' or row['extension'] == 'doc'):
            img = 'word.png'
        elif(row['extension'] == 'xlsx' or row['extension'] == 'xlsm' or row['extension'] == 'xls' or row['extension'] == 'csv'):
            img = 'excel.png'
        elif(row['extension'] == 'pdf'):
            img = 'pdf.ico'

        return img
