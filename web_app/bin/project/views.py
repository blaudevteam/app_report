# coding=utf-8
import json
from datetime import date
from datetime import datetime
from flask import render_template, Blueprint, request, session, redirect, url_for
from flask_paginate import get_page_parameter, Pagination
from flask_login import LoginManager, login_required, logout_user, login_user
from werkzeug.utils import secure_filename
import os

from .forms import ProjectFilterForm,CreateNewProject,EditProjectForm,AddSprintForm,AddTaskForm,NewSprintProject,NewDocumentModule

from web_app import app, conn
from web_app.bin.project.models import ProjectDao
from web_app.bin.client.models import ClientDao
from web_app.bin.user.models import UserDao

items_for_page = app.config["ITEMS_FOR_PAGE"]
value_journey = app.config["VALUE_JOURNEY"]
#######################
#### Configuration ####
#######################



project_blueprint = Blueprint('project', __name__)

project_dao = ProjectDao(conn)
client_dao = ClientDao(conn)
user_dao = UserDao(conn)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg','docx','docm','dotx','doc','xlsx','xlsm','xls','pdf','csv'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


ALLOWED_EXTENSIONS_EXCEL = set(['xlsx'])

def allowed_file_excel(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_EXCEL


################
#### routes ####
################




@project_blueprint.route('/projects', methods=['GET', 'POST'])
@login_required
def projects():
    error = request.args.get('error',None)
    form = ProjectFilterForm(request.form)

    if session['rol'] == 'developer' or session['rol']=='cliente' :
        return redirect(url_for('user.home'))



    if request.method == 'POST' and form.validate_on_submit():
        try:
            name = form.name.data
            state = form.state.data
            href = url_for("project.projects", name=name,state=state)
            href += "&page={0}"
        except Exception as e:
            print(e)
    else:
        name = request.args.get("name", '')
        state = request.args.get("state", '')
        href = url_for("project.projects", name=name,state=state)
        href += "&page={0}"

    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page

    if session['rol'] == 'gerente':
        user = None
    else:
        user = session['id_user']

    projects = project_dao.get_projects(user,name,state,offset,items_for_page)
    count_file_projects = project_dao.get_projects_count(user,name,state)

    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_file_projects,
                            per_page_parameter=items_for_page,record_name='projects',href=href)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/project/projects.html',projects=projects,pagination=pagination,form=form
    ,count_projects=count_projects,count_notifications=count_notifications,error=error)

@project_blueprint.route('/new_project', methods=['GET', 'POST'])
@login_required
def new_project():
    error=None
    info = None

    form = CreateNewProject(request.form)

    if session['rol'] != 'gestor' or session['rol']=='cliente':
        return redirect(url_for('user.home'))

    if request.method == 'POST' and form.validate_on_submit():
        id_admin = session['id']
        result = project_dao.add_project(id_admin,form.name.data,"0",form.description.data)

        if result :
            info = 'El proyecto se agrego correctamente'
            project = project_dao.get_project_by_name(form.name.data)
            return redirect(url_for('project.project',id=str(project['id'])))
        else :
            error = "El proyecto ya existe"

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/project/new_project.html',form=form,info=info,error=error,count_projects=count_projects
    ,count_notifications=count_notifications)

@project_blueprint.route('/project/<id>/edit', methods=['GET', 'POST'])
@login_required
def edit_project(id):

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))
    error = None
    info = None

    user_name = session['id']
    result_admin = project_dao.match_admin_project(id,user_name)
    if (result_admin):
        pass
    else:
        result_member = project_dao.match_member_project(id,user_name)
        if(result_member):
            pass
        else:
            error = 'No tienes acceso al proyecto'
            return redirect(url_for('project.projects',error=error))

    form = EditProjectForm(request.form)

    project = project_dao.get_project(id)

    if request.method == 'POST':

        project_dao.update_project(id,form.name.data,form.description.data)

        user_name = session['id']
        message = '{} actualizo el proyecto'.format(user_name)

        name = 'Actualizacion de proyecto'
        project_dao.new_record(id,user_name,name,message)

        info = "Se actualizo correctamente el proyecto"

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/project/edit_project.html',project=project,form=form,error=error,info=info,count_projects=count_projects
    ,count_notifications=count_notifications)

@project_blueprint.route('/delete_project', methods=['GET', 'POST'])
@login_required
def delete_project():
    id = request.form['id']
    project_dao.delete_project(id)
    return "result"


@project_blueprint.route('/finalize_project', methods=['GET', 'POST'])
@login_required
def finalize_project():

    id_project = request.form['id']
    project_dao.finalize_project(id_project)

    id_project = request.form['id']
    id_user = session['id_user']
    user_name = session['id']
    project = project_dao.get_project(id_project)
    id_admin = str(project['admin'])

    name = 'Actualizacion de proyecto'
    message = '{} finalizo  el proyecto : {} '.format(user_name,project['nombre'])
    to_admin = True
    user_dao.add_notification(name,message,to_admin,id_admin = id_admin)

    name = 'Actualizacion de proyecto'
    message = '{} finalizo el proyecto'.format(user_name)
    project_dao.new_record(id_project,user_name,name,message)


    return ""



@project_blueprint.route('/project/<id>', methods=['GET', 'POST'])
@login_required
def project(id):
    info = request.args.get('info',None)
    error = request.args.get('error',None)
    creation = datetime.today()

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    user_name = session['id']
    if session['rol'] == 'gerente':
        result_admin = True
    else:
        result_admin = project_dao.match_admin_project(id,user_name)
    if (result_admin):
        pass
    else:
        result_member = project_dao.match_member_project(id,user_name)
        if(result_member):
            pass
        else:
            return redirect(url_for('user.home'))


    project = project_dao.get_project(id)

    if(project['estado']=='Pendiente'):
        return redirect(url_for('project.projects'))

    admin = user_dao.get_user(project['admin'])

    sprints = project_dao.get_sprints(id)
    # app.logger.debug(sprints)

    count_members = project_dao.get_count_members(id)

    count_sprints = project_dao.get_sprints_count(id)


    count_pendientes = project_dao.get_sprints_pendiente(id)
    count_desarrollo = project_dao.get_sprints_desarrollo(id)
    count_finales = project_dao.get_sprints_final(id)

    sprint_pending = count_pendientes + count_desarrollo

    if (count_sprints != 0):
        percent = ((float(project['horas_invertidas']) * 100) / float(project['horas_estimadas']) )
    else:
        percent = 0

    records = project_dao.get_records(id)

    members = project_dao.get_members(id)

    count_sprints_project = project_dao.get_sprints_project_count(id)
    sprints_project = project_dao.get_sprints_project(id)


    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/project/project.html',project=project,admin=admin,sprints=sprints,count_members=count_members
    ,count_sprints=count_sprints,count_pendientes=count_pendientes,count_desarrollo=count_desarrollo
    ,count_finales=count_finales,percent=percent,records=records,count_projects=count_projects
    ,count_notifications=count_notifications,members=members,sprint_pending=sprint_pending
    ,count_sprints_project=count_sprints_project,sprints_project=sprints_project,info=info,error=error)


@project_blueprint.route('/add_member', methods=['GET', 'POST'])
@login_required
def add_member():

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    user_name = request.form['user_name']
    id_project = request.form['id_project']
    name_user = user_name.lower()
    name_user = name_user.replace(' ', '')

    user = user_dao.get_user_by_username(name_user)
    if(user['rol'] == "gerente"):
        return "rol"

    result = project_dao.add_member(user_name,id_project)
    if (result):
        project = project_dao.get_project(id_project)
        name = 'Union a proyecto'
        message = 'Se te agrego al proyecto {}'.format(project['nombre'])
        to_admin = False
        user_dao.add_notification(name,message,to_admin,id_user = str(user['id']))

    return "{}".format(result)


@project_blueprint.route('/match_member_project', methods=['GET', 'POST'])
@login_required
def match_member_project():

    id_project = request.form['id_project']
    user_name = session['id']

    result = project_dao.match_member_project(id_project,user_name)

    return "{}".format(result)


@project_blueprint.route('/match_admin_project', methods=['GET', 'POST'])
@login_required
def match_admin_project():

    id_project = request.form['id_project']
    user_name = session['id']
    if session['rol'] == 'gerente':
        result = True
    else:
        result = project_dao.match_admin_project(id_project,user_name)


    return "{}".format(result)



@project_blueprint.route('/add_sprint/<id>', methods=['GET', 'POST'])
@login_required
def add_sprint(id):

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))


    info = None
    error = None

    project = project_dao.get_project(id)
    if (project['estado'] == "Finalizado"):
        error = 'El proyecto ya fue finalizado'
        return redirect(url_for('project.projects',error = error))


    user_name = session['id']
    result_admin = project_dao.match_admin_project(id,user_name)
    if (result_admin):
        pass
    else:
        result_member = project_dao.match_member_project(id,user_name)
        if(result_member):
            pass
        else:
            return redirect(url_for('user.home'))

    form = AddSprintForm(request.form)

    if request.method == 'POST':
        if (form.flag_journeys.data == "0"):
            project_dao.add_sprint(id,form.name.data,form.state.data,form.date.data,form.time.data,form.sprint.data,form.priority.data,form.description.data
            ,False,'---','---',0,'',False)
        else:
            project_dao.add_sprint(id,form.name.data,form.state.data,form.date.data,0,form.sprint.data,form.priority.data,form.description.data
            ,False,'---','---',form.journeys.data,'',True)
        info = 'Modulo agregado con exito'

        project_dao.update_time_project(id,form.flag_journeys.data,form.time.data,form.journeys.data,value_journey)

        user_name = session['id']
        message = '{} creo el modulo : {} '.format(user_name,form.name.data)
        name = 'Creacion de modulo'
        project_dao.new_record(id,user_name,name,message)

    count_sprints_project = project_dao.get_sprints_project_count(id)
    sprints_project = project_dao.get_sprints_project(id)


    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/project/add_sprint.html',form=form,id=id,info=info,error=error,count_projects=count_projects
    ,count_notifications=count_notifications,count_sprints_project=count_sprints_project
    ,sprints_project=sprints_project)

@project_blueprint.route('/add_task/<id_sprint>/<id_project>', methods=['GET', 'POST'])
@login_required
def add_task(id_sprint,id_project):
    info = None
    error = None
    form = AddTaskForm(request.form)

    if session['rol'] == 'gerente' or session['rol']=='cliente':
        return redirect(url_for('user.home'))

    sprint = project_dao.get_sprint(id_sprint)
    if sprint['estado'] == "Finalizado":
        return redirect(url_for('user.home'))

    result = project_dao.match_developer_sprint(session['id_user'],id_sprint)
    if (session['rol']=='gestor'):
        result = True
    if (result == False):
        return redirect(url_for('user.home'))

    if request.method == 'POST' and form.validate_on_submit():
        id_user = session['id_user']
        project_dao.add_task(id_project,id_user,id_sprint,form.name.data,form.time.data,form.coment.data)
        info = 'Se agrego la tarea correctamente'

        user_name = session['id']
        message = '{} agrego la tarea  : {} '.format(user_name,form.name.data)
        name = 'Tarea'
        project_dao.new_record(id_project,user_name,name,message)


    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('/views/project/add_task.html',form=form
    ,id_project=id_project,count_projects=count_projects,count_notifications=count_notifications,info=info,error=error)

@project_blueprint.route('/add_sprint_project/<id_project>', methods=['GET', 'POST'])
@login_required
def add_sprint_project(id_project):

    info=None
    error=None

    user_name = session['id']
    result_admin = project_dao.match_admin_project(id_project,user_name)
    if (result_admin == False):
        return redirect(url_for('user.home'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    form = NewSprintProject(request.form)

    if request.method == 'POST' and form.validate_on_submit():
        result = project_dao.add_sprint_project(id_project,form.name.data)

        if (result):
            error = 'El sprint ya se encuentra creado'
        else:
            name = 'Creacion de Sprint'
            message = '{} creo el sprint: {}'.format(user_name,form.name.data)
            project_dao.new_record(id_project,user_name,name,message)
            info = 'Se ha creado el sprint {} exitosamente'.format(form.name.data)

    return render_template('views/project/new_sprint_project.html',id_project=id_project,info=info
    ,error=error,count_notifications=count_notifications,count_projects=count_projects,form=form)

@project_blueprint.route('/edit_sprint_project/<id_sprint>/<id_project>', methods=['GET', 'POST'])
@login_required
def edit_sprint_project(id_sprint,id_project):

    info=None
    error=None

    user_name = session['id']
    result_admin = project_dao.match_admin_project(id_project,user_name)
    if (result_admin == False):
        return redirect(url_for('user.home'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    sprint = project_dao.get_sprint_project(id_sprint)

    form = NewSprintProject(request.form)

    if request.method == 'POST':
        result = project_dao.update_sprint_project(id_sprint,form.name.data)
        if (result):
            error = 'El sprint ya existe'
        else:
            info = 'Se ha actualizado el sprint'


    return render_template('views/project/edit_sprint_project.html',form=form,info=info,error=error
    ,sprint=sprint,id_project=id_project,count_notifications=count_notifications
    ,count_projects=count_projects)

@project_blueprint.route('/edit_module/<id_project>/<id_module>', methods=['GET', 'POST'])
@login_required
def edit_module(id_project,id_module):

    info = None
    error = None

    user_name = session['id']
    result_admin = project_dao.match_admin_project(id_project,user_name)
    if (result_admin == False):
        return redirect(url_for('user.home'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    form = AddSprintForm(request.form)

    module = project_dao.get_sprint(id_module)
    count_sprints_project = project_dao.get_sprints_project_count_unfiltered(id_project)
    sprints_project = project_dao.get_sprints_project_unfiltered(id_project)

    if request.method == 'POST':
        if (form.priority.data != "None"):
            priority = form.priority.data
        elif (form.priority2.data != "None"):
            priority = form.priority2.data
        else:
            priority = form.priority3.data

        if (form.flag_journeys.data == "0"):
            project_dao.update_module(id_module,form.name.data,form.time.data,form.sprint.data,module['flag_cliente'],"0",priority,False)
        else:
            project_dao.update_module(id_module,form.name.data,"0",form.sprint.data,module['flag_cliente'],form.journeys.data,priority,True)

        project_dao.update_time_project(id_project,form.flag_journeys.data,form.time.data,form.journeys.data,value_journey)

        info = ' Se ha actualizado la informacion'


    return render_template('views/project/edit_module.html',info=info,error=error
    ,form=form,module=module,id_project=id_project,count_notifications=count_notifications
    ,count_projects=count_projects,count_sprints_project=count_sprints_project
    ,sprints_project=sprints_project)

@project_blueprint.route('/add_docuemnt/<id_module>/<id_project>', methods=['GET', 'POST'])
@login_required
def add_docuemnt(id_module,id_project):

    result = 'no_permitido'
    if session['rol'] == 'gerente' :
        result = 'gerente'
    else:
        result_match = client_dao.match_user_module(id_module,session['id_user'])
        if (result_match):
            result = 'permitido'
        else:
            result_admin = project_dao.match_admin_project(id_project,session['id'])
            if (result_admin):
                result = 'permitido'

    if (result == 'no_permitido'):
        error = 'No tiene permitido agregar documentos a este modulo'
        return redirect(url_for('project.project',id=id_project,error=error))

    info = None
    error = None
    form = NewDocumentModule(request.form)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST'  :
        if 'document' not in request.files:
            error ='El arhivo no existe'
            return render_template('views/project/add_docuemnt.html',info=info,error=error,form=form,count_projects=count_projects
            ,count_notifications=count_notifications,id_project=id_project)

        file = request.files['document']
        if file.filename == '':
            error = 'Archivo no seleccionado'
            return render_template('views/project/add_docuemnt.html',info=info,error=error,form=form,count_projects=count_projects
            ,count_notifications=count_notifications,id_project=id_project)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            result = project_dao.add_docuemnt_module(id_module,filename)
            if (result):
                error = 'El documento ya se encuentra en los registros'
            else:
                file.save(os.path.join(app.config['UPLOAD_FOLDER_DOCUMENTS'], filename))
                info = 'Se ha subido el documento {}'.format(filename)
                return redirect(url_for('project.project',id=id_project,info=info))
        else:
            error = 'El archivo no es compartible'

    return render_template('views/project/add_docuemnt.html',info=info,error=error,form=form,count_projects=count_projects
    ,count_notifications=count_notifications,id_project=id_project)



@project_blueprint.route('/upload_excel_modules/<id_project>', methods=['GET', 'POST'])
@login_required
def upload_excel_modules(id_project):
    info = None
    error = None
    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST':
        if 'file' not in request.files:
            error = 'El archivo no existe'
            return render_template('views/project/upload_excel_modules.html',count_notifications=count_notifications,count_projects=count_projects,id_project=id_project
            ,info = info, error = error)

        file = request.files['file']

        if file.filename == '':
            error = 'El archivo no existe'
            return render_template('views/project/upload_excel_modules.html',count_notifications=count_notifications,count_projects=count_projects,id_project=id_project
            ,info = info, error = error)
        if file and allowed_file_excel(file.filename):
            project_dao.upload_excel_modules(file,id_project,value_journey)
            info = 'Modulos cargados correctamente'
            return redirect(url_for('project.project',id=id_project,info=info))

    return render_template('views/project/upload_excel_modules.html',count_notifications=count_notifications,count_projects=count_projects,id_project=id_project
    ,info = info, error = error)


@project_blueprint.route('/update_sprint_state', methods=['GET', 'POST'])
@login_required
def update_sprint_state():
    id_sprint = request.form['id']
    new_state = request.form['new_state']
    id_project = request.form['id_project']
    horas = request.form['horas']

    project_dao.update_sprint_state(id_sprint,new_state,horas)

    sprint = project_dao.get_sprint(id_sprint)

    user_name = session['id']

    if (new_state == "En proceso"):
        name = 'Actualizacion de modulo'
        message = '{} cambio el modulo : {} a proceso'.format(user_name,sprint['nombre'])
    if (new_state == "Finalizado"):
        name = 'Finalizacion de modulo'
        message = '{} finalizo el modulo : {}'.format(user_name,sprint['nombre'])


    project_dao.new_record(id_project,user_name,name,message)

    return ""

@project_blueprint.route('/delete_task', methods=['GET', 'POST'])
@login_required
def delete_task():
    id_sprint = request.form['id_sprint']
    id_task = request.form['id']
    id_project = request.form['id_project']

    if (session['rol']!="developer"):
        return "permiso_denegado"

    sprint = project_dao.get_sprint(id_sprint)
    if (sprint['estado']=="Finalizado"):
        return "sprint_finalizada"

    result_match  = project_dao.match_developer_sprint(session['id_user'],id_sprint)
    if(result_match == False):
        return "no_asignado"

    user_name = session['id']
    task = project_dao.get_task(id_task)
    message = '{} elimino la tarea : {} '.format(user_name,task['nombre'])
    name = 'Eliminacion de tarea'
    project_dao.new_record(id_project,user_name,name,message)

    project_dao.delete_task(id_task,id_project)

    return "True"

@project_blueprint.route('/delete_sprint', methods=['GET', 'POST'])
@login_required
def delete_sprint():
    id = request.form['id']
    id_project = request.form['id_project']

    sprint = project_dao.get_sprint(id)
    user_name = session['id']
    message = '{} elimino el modulo  : {} '.format(user_name,sprint['nombre'])
    name = 'Eliminacion de modulo'
    project_dao.new_record(id_project,user_name,name,message)

    project_dao.delete_sprint(id,id_project)

    module = project_dao.get_sprint(id)
    project_dao.delete_time_project(id_project,module['flag_jornada'],module['horas_estimadas'],module['jornada'],value_journey)

    return ""


@project_blueprint.route('/project_to_development', methods=['GET', 'POST'])
@login_required
def project_to_development():

    id_project = request.form['id_project']
    project_dao.project_to_development(id_project)
    id_user = session['id_user']
    user_name = session['id']
    project = project_dao.get_project(id_project)

    name = 'Actualizacion de proyecto'
    message = '{} regreso el proyecto : {} a desarrollo '.format(user_name,project['nombre'])
    to_admin = False
    user_dao.add_notification(name,message,to_admin,id_user = id_user)

    name = 'Actualizacion de proyecto'
    message = '{} actualizo el proyecto a desarrollo'.format(user_name)
    project_dao.new_record(id_project,user_name,name,message)

    return ""

@project_blueprint.route('/update_project_to_development', methods=['GET', 'POST'])
@login_required
def update_project_to_development():

    id_project = request.form['id_project']
    project_dao.update_project_to_development(id_project)

    return ""


@project_blueprint.route('/remove_member', methods=['GET', 'POST'])
@login_required
def remove_member():
    id_member = request.form['id_member']
    id_user = request.form['id_user']
    id_project = request.form['id_project']


    project = project_dao.get_project(id_project)


    project_dao.remove_member(id_member)

    name = 'Expulsion'
    message = 'Se te ha expulsado del proyecto : {}'.format(project['nombre'])
    to_admin = False
    user_dao.add_notification(name,message,to_admin,id_user = id_user)
    return ""



@project_blueprint.route('/my_projects', methods=['GET', 'POST'])
@login_required
def my_projects():
    id_user = session['id_user']

    if session['rol'] == 'gerente':
        result = project_dao.get_projects()
        jsonarray = json.dumps(result)
        return jsonarray

    result = project_dao.get_my_projects(id_user)
    jsonarray = json.dumps(result)
    return jsonarray


@project_blueprint.route('/get_detail_sprint', methods=['GET', 'POST'])
@login_required
def get_detail_sprint():
    id = request.form['id']
    result = project_dao.get_detail_sprint(id)
    jsonarray = json.dumps(result)
    return jsonarray


@project_blueprint.route('/get_tasks_sprint', methods=['GET', 'POST'])
@login_required
def get_tasks_sprint():
    id = request.form['id']
    result = project_dao.get_tasks_sprint(id)
    jsonarray = json.dumps(result)
    return jsonarray

@project_blueprint.route('/validate_sprint_to_finished', methods=['GET', 'POST'])
@login_required
def validate_sprint_to_finished():
    id = request.form['id']
    result = project_dao.validate_sprint_to_finished(id)
    return "{}".format(result)


@project_blueprint.route('/add_developer_to_project', methods=['GET', 'POST'])
@login_required
def add_developer_to_project():
    id = request.form['id_sprint']
    id_project = request.form['id_project']
    user_name = request.form['user_name']
    module = request.form['modulee']

    result = project_dao.add_developer_to_project(id,user_name,id_project)

    user = user_dao.get_user_by_username(user_name)
    gestor = session['id']
    name = 'Asignacion de tarea'
    message = '{} te asigno al modulo : {} '.format(gestor,module)
    to_admin = False
    user_dao.add_notification(name,message,to_admin,id_user = str(user['id']))
    return "{}".format(result)


@project_blueprint.route('/match_developer_sprint', methods=['GET', 'POST'])
@login_required
def match_developer_sprint():
    id = session['id_user']
    id_sprint = request.form['id_sprint']
    result = project_dao.match_developer_sprint(id,id_sprint)
    rol = session['rol']
    if (rol == 'gestor'):
        result = True
    return "{}".format(result)


@project_blueprint.route('/get_developers_sprint', methods=['GET', 'POST'])
@login_required
def get_developers_sprint():
    id = request.form['id']

    result = project_dao.get_developers_sprint(id)
    jsonarray = json.dumps(result)
    return jsonarray


@project_blueprint.route('/delete_develop_sprint', methods=['GET', 'POST'])
@login_required
def delete_develop_sprint():

    id_sprint = request.form['id_sprint']
    id_user = request.form['id_user']
    id_project = request.form['id_project']

    project_dao.delete_develop_sprint(id_sprint,id_user,id_project)

    return ""

@project_blueprint.route('/delete_sprint_project', methods=['GET', 'POST'])
@login_required
def delete_sprint_project():

    id = request.form['id']
    id_project = request.form['id_project']
    sprint = project_dao.get_sprint_project(id)

    user_name = session['id']
    name = 'Eliminacion de Sprint'
    message = '{} elimino el sprint: {}'.format(user_name,sprint['nombre'])
    project_dao.new_record(id_project,user_name,name,message)

    project_dao.delete_sprint_project(id)



    return ""

@project_blueprint.route('/get_comments_module', methods=['GET', 'POST'])
@login_required
def get_comments_module():

    id = request.form['id']

    comments = project_dao.get_comments_module(id)
    jsonarray = json.dumps(comments)
    return jsonarray

@project_blueprint.route('/add_comment_module', methods=['GET', 'POST'])
@login_required
def add_comment_module():


    id_module = request.form['id_module']
    message = request.form['message']

    project_dao.add_comment_module(id_module,message,session['id_user'])

    return ''

@project_blueprint.route('/validate_new_comment_module', methods=['GET', 'POST'])
@login_required
def validate_new_comment_module():


    id = request.form['id']
    result = project_dao.validate_new_comment_module(id)
    jsonarray = json.dumps(result)
    return jsonarray


@project_blueprint.route('/match_user_comment', methods=['GET', 'POST'])
@login_required
def match_user_comment():


    id = request.form['id']
    result = project_dao.match_user_comment(id,session['id_user'])

    return '{}'.format(result)

@project_blueprint.route('/delete_comment_module', methods=['GET', 'POST'])
@login_required
def delete_comment_module():


    id = request.form['id']
    project_dao.delete_comment_module(id)
    return ''

@project_blueprint.route('/get_documents_module', methods=['GET', 'POST'])
@login_required
def get_documents_module():


    id = request.form['id']
    documents = project_dao.get_documents_module(id)

    jsonarray = json.dumps(documents)
    return jsonarray
