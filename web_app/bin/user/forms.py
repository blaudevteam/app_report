# coding=utf-8
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField,SelectField,FileField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    user = StringField('Usuario*', validators=[DataRequired(message='Debe ingresar el usuario')])
    password = PasswordField('Password*', validators=[DataRequired(message='Debe ingresar la clave')])


class CreateNewUserForm(FlaskForm):
    user = StringField('Usuario*', validators=[DataRequired(message='Debe ingresar el usuario')])
    name = StringField('Nombre*', validators=[DataRequired(message='Debe ingresar el nombre')])
    cost_hour = StringField('Costo Hora*', validators=[DataRequired(message='Debe ingresar el costo por hora')])
    rol = SelectField('Rol:',choices=[('gerente', 'Gerente'),('gestor', 'Gestor'),('developer', 'Developer')])
    password = PasswordField('Clave*', validators=[DataRequired(message='Debe ingresar la clave')])
    email = StringField('Correo*', validators=[DataRequired(message='Debe ingresar el correo')])


class UserFilterForm(FlaskForm):
    user = StringField('Usuario', validators=[])
    user_name = StringField('Nombre', validators=[])
    email = StringField('Correo', validators=[])
    rol = SelectField('Rol:',choices=[('', 'Todos'),('gerente', 'Gerentes'),('gestor', 'Gestor'),('developer', 'Developer')])
    state = SelectField('Estado:',choices=[('all', 'Ambos'),('Activo', 'Activo'),('Inactivo', 'Inactivo')])


class UserEditForm(FlaskForm):
    name = StringField('Usuario*', validators=[DataRequired(message='Debe ingresar nombre')])
    user = StringField('Usuario*', validators=[DataRequired(message='Debe ingresar el usuario')])
    user_name = StringField('Nombre*', validators=[DataRequired(message='Debe ingresar el nombre')])
    user_name_actual = StringField('Nombre*', validators=[])
    email = StringField('Correo*', validators=[DataRequired(message='Debe ingresar el correo')])
    cost_hour = StringField('Costo Hora*', validators=[DataRequired(message='Debe ingresar el costo por hora')])
    rol_gerent = SelectField('Estado:',choices=[('gerente', 'Gerente'),('gestor', 'Gestor'),('developer', 'Developer')])
    rol_gestor = SelectField('Estado:',choices=[('gestor', 'Gestor'),('gerente', 'Gerente'),('developer', 'Developer')])
    rol_developer = SelectField('Estado:',choices=[('developer', 'Developer'),('gerente', 'Gerente'),('gestor', 'Gestor')])
    rol_admin = SelectField('Estado:',choices=[('admin', 'Admin'),('usuario', 'Usuario')])
    rol_user = SelectField('Estado:',choices=[('usuario', 'Usuario'),('admin', 'Admin')])

class UserEditMeForm(FlaskForm):
    email = StringField('Correo*', validators=[DataRequired(message='Debe ingresar el correo')])
    exist_password = StringField('Clave*', validators=[])
    old_password = StringField('Clave*', validators=[])
    new_password = StringField('Clave*', validators=[])


class ChagePasswordForm(FlaskForm):
    password = StringField('Clave*', validators=[DataRequired(message='Debe ingresar la clave')])
    new_password = StringField('Nueva clave*', validators=[DataRequired(message='Debe ingresar la clave')])

class ForgetPasswordForm(FlaskForm):
    user = StringField('Usuario*', validators=[DataRequired(message='Debe ingresar al usuario')])

class RecoverPasswordForm(FlaskForm):
    password = PasswordField('Clave*', validators=[DataRequired(message='Debe ingresar la clave')])
    password_match = PasswordField('Repetir Clave*', validators=[DataRequired(message='Debe ingresar la clave')])

class FormPhotoProfile(FlaskForm):
    photo = FileField('Imagen*', validators=[DataRequired(message='Debe ingresar la imagen')])





def validateSelectFile(state):
    if state :
        if state =='Activo':
            return True
        elif state == 'Inactivo':
            return False
        else:
            return None

    else:
        return None
