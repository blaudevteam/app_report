import smtplib
import random
import pandas as pd
from bson import ObjectId
from flask_login import UserMixin
from web_app import bcrypt,app
from datetime import datetime



class UserDao:

    def __init__(self, conn):
        self.db_user = conn

    def get_user_auth(self, id_user):
        user = self.db_user.db.usuarios.find_one({"user_name": id_user})
        if user:
            return User(user["user_name"],user["nombre"], user["password"],user["email"], user["active"],user["rol"],user['_id'],user['img'])
        else:
            user = self.db_user.db.clientes.find_one({"user_name": id_user})
            if user:
                return User(user["user_name"],user["nombre"], user["password"],user["email"], user["active"],user["rol"],user['_id'],user['img'])
            return None

    def get_user(self, id_user):
        user_db = self.db_user.db.usuarios.find_one({"_id": ObjectId(id_user)})
        if user_db:
            user = {"id": user_db['_id'],
                    "user": user_db['user_name'],
                    "nombre": user_db['nombre'],
                    "password": user_db['password'],
                    "email": user_db['email'],
                    "rol": user_db['rol'],
                    "img": user_db['img'],
                    "costo_hora": user_db['costo_hora'],
                    "active": user_db['active']}
            return user
        else:
            app.logger.debug('El usuario no existe')

    def get_admins(self):
        users_db = self.db_user.db.usuarios.find({"rol":"admin"})
        users = [{"id": str(file_rec['_id']),
                  "user_name": file_rec['user_name'],
                  "password": file_rec['password'],
                  "email": file_rec['email'],
                  "rol": file_rec['rol'],
                  "active": file_rec['active']} for file_rec in users_db]
        return users

    def get_user_by_username(self,user_name):
        name = user_name.lower()
        name = name.replace(' ', '')
        user_db = self.db_user.db.usuarios.find_one({"user_name": name})
        if user_db:
            user = {"id": user_db['_id'],
                    "user": user_db['user_name'],
                    "password": user_db['password'],
                    "email": user_db['email'],
                    "rol": user_db['rol'],
                    "active": user_db['active']}
            return user
        else:
            app.logger.debug('El usuario no existe')

    def get_users(self,user=None,user_name=None,email=None,rol=None,state=None,offset=0,items_pages=0):
        query = self.__build_query(user,user_name,email,rol,state)

        users_db = self.db_user.db.usuarios.find(query).skip(offset).limit(items_pages)
        users = [{"id": str(file_rec['_id']),
                  "user_name": file_rec['user_name'],
                  "nombre": file_rec['nombre'],
                  "password": file_rec['password'],
                  "email": file_rec['email'],
                  "rol": file_rec['rol'],
                  "cost_hour": file_rec['costo_hora'],
                  "active": file_rec['active']} for file_rec in users_db]

        return users

    def get_users_count(self,user=None,user_name=None,email=None,rol=None,state=None):
        query = self.__build_query(user,user_name,email,rol,state)
        return self.db_user.db.usuarios.find(query).count()

    def add_user(self, user_id=None,name=None,email=None,password=None,active=None,cost_hour=None,rol=None,cost_journey=None,img_user=None):
        user = self.db_user.db.usuarios.find_one({"user_name": user_id})
        client = self.db_user.db.clientes.find_one({"user_name": user_id,"activo":True})
        if (user or client):
            return False
        else:
            self.db_user.db.usuarios.insert({"user_name": user_id,"nombre":name,"email": email
            , "active": active,"rol":rol,"costo_hora":cost_hour,"costo_jornada":cost_journey,"img":img_user
            ,"password": bcrypt.generate_password_hash(password)})
            return True

    def delete_user(self, id_user):
        self.db_user.db.usuarios.update_one({"_id": ObjectId(id_user)}, {"$set": {"active": False}})
        user = self.get_user(id_user)
        if(user['rol'] == "developer"):
            members_count = self.db_user.db.miembros.find({"id_usuario":id_user,"activo":True}).count()
            if (members_count > 0):
                members_db = self.db_user.db.miembros.find({"id_usuario":id_user,"activo":True})
                members = [{"id": str(file_rec['_id'])} for file_rec in members_db]
                df_members = pd.DataFrame(members)
                df_members['delete_member'] = df_members.apply(lambda x: self.__delete_member_project(x), axis=1)


    def update_user(self, user_id,name,email,rol,cost_hour):
        self.db_user.db.usuarios.update_one({"_id": ObjectId(user_id)}, {"$set": {"nombre": name, "email": email
        , "rol": rol,"costo_hora":cost_hour}})
        return True

    def change_password(self,id,old_password,new_password):
        user = self.get_user(id)
        if bcrypt.check_password_hash(user["password"], old_password):
             self.db_user.db.usuarios.update_one({"_id": ObjectId(id)}
             ,{"$set": {"password": bcrypt.generate_password_hash(new_password)}})

             return True
        else:
            return False


    def update_active_user(self,id,active):
        self.db_user.db.usuarios.update_one({"_id": ObjectId(id)}, {"$set": {"active": active}})


    def validate_user_exist(self,user_name):
        user = self.db_user.db.usuarios.find_one({"user_name": user_name})
        if user:
            return True
        else:
            return False

    def add_notification(self,name=None, message=None, to_admin=None,id_admin=None,id_user=None,to_admins=None):
        if (to_admin):
            creation = datetime.today()
            self.db_user.db.notificaciones.insert({
            "nombre": name
            ,"descripcion":message
            ,"fecha_creacion":creation
            ,"id_usuario":id_admin
            ,"estado":"pendiente"
            ,"activo":True

            })
        else:
            creation = datetime.today()
            self.db_user.db.notificaciones.insert({
            "nombre": name
            ,"descripcion":message
            ,"fecha_creacion":creation
            ,"id_usuario":id_user
            ,"estado":"pendiente"
            ,"activo":True

            })

        if (to_admins):
            users = self.get_admins()
            for user in users:
                creation = datetime.today()
                self.db_user.db.notificaciones.insert({
                "nombre": name
                ,"descripcion":message
                ,"fecha_creacion":creation
                ,"id_usuario":str(user['id'])
                ,"estado":"pendiente"
                ,"activo":True
                })

    def get_notifications(self,user_name):
        user = self.get_user_by_username(user_name)
        notifications_db = self.db_user.db.notificaciones.find({"id_usuario":str(user['id']),"activo":True })
        notifications = [{"id": str(file_rec['_id']),
                  "nombre": file_rec['nombre']
                  ,"descripcion" : file_rec['descripcion']
                  ,"fecha_creacion" : file_rec['fecha_creacion'].strftime("%d-%m-%Y %H:%M")
                  ,"id_usuario" : file_rec['id_usuario']
                  ,"estado" : file_rec['estado']
                  ,"activo" : file_rec['activo']
                  } for file_rec in notifications_db]

        return notifications

    def get_notifications_count(self,user_name):
        user = self.get_user_by_username(user_name)
        return self.db_user.db.notificaciones.find({"id_usuario":str(user['id']),"activo":True,"estado": "pendiente" }).count()

    def notify_checked(self,id):
        self.db_user.db.notificaciones.update_one({"_id": ObjectId(id)}, {"$set": {"estado": "visto"}})

    def delete_notification(self,id):
        self.db_user.db.notificaciones.update_one({"_id": ObjectId(id)}, {"$set": {"activo": False}})

    def update_me(self,id_user=None,email=None,old_password=None,new_password=None,confirm_pass=None):
        if (confirm_pass == "True"):
            user = self.get_user(id_user)
            if bcrypt.check_password_hash(user["password"], old_password):
                 self.db_user.db.usuarios.update_one({"_id": ObjectId(id_user)}
                 ,{"$set": {"password": bcrypt.generate_password_hash(new_password)}})
                 return True
            else:
                return False
        else:
             self.db_user.db.usuarios.update_one({"_id": ObjectId(id_user)}
             ,{"$set": {"email":email}})
             return True


    def forget_passowrd_validation(self,user_name):
        user = self.get_user_by_username(user_name)
        if (user):
            recover_db = self.db_user.db.recuperaciones.find_one({"id_usuario": str(user['id']),"activo":True})
            if(recover_db):
                return False
            else:
                strin_random = [random.choice('abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') for _ in range(25)]
                token = ''.join(strin_random)
                self.__send_email(str(user['id']),user['email'],token)
                self.db_user.db.recuperaciones.insert({"id_usuario": str(user['id']),"token":token,"activo":True})
                return True
        else:
            return False

    def match_user_token_password(self,token,id):
        match_token_db = self.db_user.db.recuperaciones.find_one({"id_usuario":id,"token":token,"activo":True})
        if(match_token_db):
            return True
        else:
            return False

    def update_password_user(self,id,password):
         self.db_user.db.usuarios.update_one({"_id": ObjectId(id)}
         ,{"$set": {"password": bcrypt.generate_password_hash(password)}})

    def update_match_token_password(self,id,token):
        self.db_user.db.recuperaciones.update_one({"id_usuario": id,"token":token}
        ,{"$set": {"activo":False}})

    def update_img_user(self,new_img,id):
         self.db_user.db.usuarios.update_one({"_id": ObjectId(id)}
         ,{"$set": {"img":new_img}})
         return True

    def __send_email(self,id_user,email,token):
        fromaddr = 'envioblau@gmail.com'
        toaddrs  = email
        server = 'Para restaurar la clave debe ir a este link \n ec2-35-172-159-146.compute-1.amazonaws.com/report/recover_password'
        msg = '{}/{}/{}'.format(server,token,id_user)
        app.logger.debug("datos %s",msg)
        # Datos
        username = 'envioblau@gmail.com'
        password = 'envioblau01@'

        # Enviando el correo
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(username,password)
        server.sendmail(fromaddr, toaddrs, msg)
        server.quit()


    def __build_query(self, user=None,user_name=None,email=None,rol=None,state=None):
        if (state == None):
            query = {"active":True}
        else:
            query = {}

        if user:
            query["user_name"] = {"$regex": user, "$options": 'i'}

        if user_name:
            query["nombre"] = {"$regex": user_name, "$options": 'i'}

        if email:
            query["email"] = {"$regex": email, "$options": 'i'}

        if rol:
            query["rol"] = {"$regex": rol, "$options": 'i'}

        if state is not None:
            if (state != 'all'):
                query["active"] = state

        return query


    def __delete_member_project(self,row):
        self.db_user.db.miembros.update_one({"_id": ObjectId(str(row['id']))}, {
            "$set": {
                "activo":False
            }})
        return "---"

class User(UserMixin):

    def __init__(self, user_name,nombre, password, email, active,rol,id_user,img):
        self.authenticated = False
        self.user_name = user_name
        self.nombre = nombre
        self.id = user_name
        self.password = password
        self.email = email
        self.active = active
        self.rol = rol
        self.id_user = id_user
        self.img = img

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def authenticate_user(self, password):
        app.logger.debug("password enviada : %s",password)
        if bcrypt.check_password_hash(self.password, password):
            self.authenticated = True
            return True
        else:
            return False

    def validate_activation(self):
        return self.active
