# coding=utf-8
from flask import render_template, Blueprint, request, session, redirect, url_for
from flask_login import LoginManager, login_required, logout_user, login_user
from flask_paginate import get_page_parameter, Pagination
from werkzeug.utils import secure_filename
import os

from web_app import app, conn
from web_app.bin.user.models import UserDao
from web_app.bin.project.models import ProjectDao
from .forms import LoginForm,CreateNewUserForm,UserFilterForm,UserEditForm,ChagePasswordForm,UserEditMeForm,ForgetPasswordForm,RecoverPasswordForm,validateSelectFile,FormPhotoProfile
items_for_page = app.config["ITEMS_FOR_PAGE"]
#######################
#### Configuration ####
#######################

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'user.login'

user_blueprint = Blueprint('user', __name__)

user_dao = UserDao(conn)
project_dao = ProjectDao(conn)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@login_manager.user_loader
def load_user(user_name):
    return user_dao.get_user_auth(user_name)


################
#### routes ####
################

@user_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    error=None
    info = None
    form = LoginForm(request.form)
    error = request.args.get('error',None)
    info = request.args.get('info',None)

    if request.method == 'POST' and form.validate_on_submit():
        user = load_user(form.user.data)
        if user is not None and user.authenticate_user(form.password.data):
            if user.validate_activation():
                login_user(user)
                session['logged_in'] = True
                session['email'] = user.email
                session['rol'] = user.rol
                session['id'] = user.id
                session['name_user'] = user.nombre
                session['img_user'] = user.img
                session['id_user'] = str(user.id_user)
                if (user.rol=='cliente'):
                    return redirect(url_for('client.homec'))
                return redirect(url_for('user.home'))
            else:
                error = 'El usuario no ha sido activado'
        else:
            app.logger.debug('El Usuario o el Password son incorrectos')
            error = 'El Usuario o el Password son incorrectos.'


    return render_template('views/user/login.html',form=form,info=info,error=error)


@user_blueprint.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    logout_user()
    return redirect(url_for('user.login'))



@user_blueprint.route('/home')
@login_required
def home():

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    id_user = session['id_user']

    my_projects = project_dao.get_my_projects(id_user)

    return render_template('views/user/home.html',count_projects=count_projects
    ,count_notifications=count_notifications,my_projects=my_projects)


@user_blueprint.route('/users',methods=['GET','POST'])
@login_required
def users():

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    if session['rol'] != 'admin' and session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    info = request.args.get('info',None)
    error = request.args.get('error',None)

    form = UserFilterForm(request.form)

    if request.method == 'POST' and form.validate_on_submit():
        try:
            user = form.user.data
            user_name = form.user_name.data
            email = form.email.data
            rol = form.rol.data
            state = form.state.data
            href = url_for("user.users", user=user, user_name=user_name, email=email,rol=rol,state=state)
            href += "&page={0}"
        except Exception as e:
            print(e)
    else:
        user = request.args.get("user", '')
        user_name = request.args.get("user_name", '')
        email = request.args.get("email", '')
        rol = request.args.get("rol", '')
        state = request.args.get("state", '')

        href = url_for("user.users", user=user, user_name=user_name, email=email,rol=rol,state=state)
        href += "&page={0}"

    if (state != "all"):
        bool_state = validateSelectFile(state)
    else:
        bool_state = 'all'
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    users = user_dao.get_users(user,user_name,email,rol,bool_state,offset,items_for_page)
    count_file_users = user_dao.get_users_count(user,user_name,email,rol,bool_state)

    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_file_users,
                            per_page_parameter=items_for_page,record_name='users',href=href)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/user/users.html',users=users,pagination=pagination,form=form,count_projects=count_projects
    ,count_notifications=count_notifications,info=info,error=error)

@user_blueprint.route('/new_user',methods=['GET','POST'])
@login_required
def new_user():


    if session['rol'] != 'admin' and session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    info = None
    error = None
    form = CreateNewUserForm(request.form)

    if request.method == 'POST' and form.validate_on_submit():
        result = user_dao.add_user(form.user.data,form.name.data,form.email.data,form.password.data,True,form.cost_hour.data,form.rol.data,'','avatar2.png')
        if result:
            info = "El Usuario {} fue creado correctamente".format(form.user.data)
            return redirect(url_for('user.users',info=info))
        else :
            error = "El Usuario {} ya existe (puede estar inactivo)".format(form.user.data)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/user/new_user.html', form=form,info=info,error=error,count_projects=count_projects
    ,count_notifications=count_notifications)


@user_blueprint.route('/user/<id>/change_password',methods=['GET','POST'])
@login_required
def change_password(id):

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    info = None
    error = None
    if session['rol'] != 'admin' and session['rol'] != 'gerente' :
        return redirect(url_for('user.home'))

    form = ChagePasswordForm(request.form)

    user = user_dao.get_user(id)

    if request.method == 'POST' and form.validate_on_submit():
        result = user_dao.change_password(id,form.password.data,form.new_password.data)
        if (result):
            info = 'Clave actualizada'
        else:
            error = 'La clave actual no es la correcta'

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    return render_template('views/user/change_password.html',form = form,count_notifications=count_notifications
    ,count_projects=count_projects,user=user,info=info,error=error)

@user_blueprint.route('/delete_user', methods=['GET', 'POST'])
@login_required
def delete_user():

    id = request.form['id']
    user_dao.delete_user(id)
    #project_dao.delete_projects_gestor(id)

    return ""


@user_blueprint.route('/user/<user_id>', methods=['GET', 'POST'])
@login_required
def edit_user(user_id):

    if session['rol'] != 'admin' and session['rol'] != 'gerente':
        return redirect(url_for('user.home'))


    info = None
    error = None

    form = UserEditForm(request.form)

    if request.method == 'POST':

        if (form.rol_gerent.data != "None"):
            rol = form.rol_gerent.data
        elif (form.rol_gestor.data != "None"):
            rol = form.rol_gestor.data
        else:
            rol = form.rol_developer.data
        result = user_dao.update_user(user_id, form.name.data,form.email.data,rol,form.cost_hour.data)
        if(result):
            info = "Se actualizo correctamente el usuario"
        else:
            error = 'El nombre de usuario ya ha sido agregado'
    try:
        user = user_dao.get_user(user_id)
    except UserNotFoundException as e:
        error = "{}".format(str(e))

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/user/edit_user.html',form=form,info=info,error=error,count_projects=count_projects
    ,count_notifications=count_notifications,user=user)




@user_blueprint.route('/register_user',methods=['GET','POST'])
def register_user():
    info = None
    error = None
    form = CreateNewUserForm(request.form)
    if request.method == 'POST' :
        result = user_dao.add_user(form.user.data,form.name.data,form.email.data,form.password.data,False,"0","developer")
        if result:
            info = "Usuario creado, espere activacion"
            name = 'Solicitud de usuario'
            message = 'El usuario {} esta solicitando unirse'.format(form.user.data)
            to_admins = True
            user_dao.add_notification(name,message,to_admins = to_admins)
        else :
            error = "El Usuario {} ya existe ".format(form.user.data)
    return render_template('views/user/register.html',form=form,info = info, error = error)

@user_blueprint.route('/forget_password',methods=['GET','POST'])
def forget_password():
    info = None
    error = None

    form = ForgetPasswordForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        result = user_dao.forget_passowrd_validation(form.user.data)
        if (result):
            user = user_dao.get_user_by_username(form.user.data)
            email = user['email']
            email_split = email.split('@')
            email_name = email_split[0]
            email_user = email_split[1]

            email_name_split = list(email_name)
            first_word = email_name_split[0]
            last_word = email_name_split[-1]

            result_email = "{}*****{}@{}".format(first_word,last_word,email_user)

            info = 'Se ha enviado un correo a {} con las instrucciones'.format(result_email)
        else:
            error = 'El usuario no existe o ya se ha enviado el correo'

    return render_template('views/user/forget_password.html',form=form,info=info,error=error)

@user_blueprint.route('/recover_password/<token>/<id>',methods=['GET','POST'])
def recover_password(token,id):
    error = None
    info = None
    result = user_dao.match_user_token_password(token,id)
    form = RecoverPasswordForm(request.form)

    if (result):
        pass
    else:
        error = 'Token invalido'
        return redirect(url_for('user.login',error=error))

    if request.method == 'POST' and form.validate_on_submit():
        password = form.password.data
        password_match = form.password_match.data
        if(password != password_match):
            error = 'Las claves no coinciden'
            return render_template('views/user/recover_password.html',form = form,info=info,error=error)
        else:
            user_dao.update_password_user(id,password)
            user_dao.update_match_token_password(id,token)
            info = 'clave actualizada con exito'
            return redirect(url_for('user.login',info=info))
    return render_template('views/user/recover_password.html',form = form,info=info,error=error)


@user_blueprint.route('/notifications',methods=['GET','POST'])
@login_required
def notifications():

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    user_name = session['id']

    notifications = user_dao.get_notifications(user_name)
    page = request.args.get(get_page_parameter(), type=int, default=1)
    offset = (items_for_page * page) - items_for_page
    count_file_notifications = user_dao.get_notifications_count(user_name)


    pagination = Pagination(css_framework='bootstrap3', page=page, total=count_file_notifications,
                            per_page_parameter=items_for_page,record_name='notifications')

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)
    return render_template('views/user/notifications.html',notifications=notifications,pagination=pagination,count_projects=count_projects
    ,count_notifications=count_notifications)


@user_blueprint.route('/edit_me',methods=['GET','POST'])
@login_required
def edit_me():

    if session['rol'] == 'cliente' :
        return redirect(url_for('client.homec'))

    error = None
    info = None
    result = False
    form = UserEditMeForm(request.form)
    id_user = session['id_user']
    user = user_dao.get_user(id_user)

    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)

    if request.method == 'POST' and form.validate_on_submit():
        email = form.email.data
        old_password = form.old_password.data
        new_password = form.new_password.data
        confirm_pass = form.exist_password.data
        if (confirm_pass=="True"):
            if (new_password == "" or old_password == ""):
                error = 'La clave esta vacia'
                return  render_template('views/user/edit_me.html',user=user,form=form
                ,count_notifications=count_notifications,count_projects=count_projects,error=error,info=info)
            else:
                result = user_dao.update_me(id_user=id_user,email=email,old_password=old_password,new_password=new_password,confirm_pass=confirm_pass)
        else:
            result = user_dao.update_me(id_user=id_user,email=email,confirm_pass=confirm_pass)

        if (result):
            info = 'Se han actualizado tus datos'
        else:
            error = 'Las claves no coinciden'

    return  render_template('views/user/edit_me.html',user=user,form=form
    ,count_notifications=count_notifications,count_projects=count_projects,error=error,info=info)

@user_blueprint.route('/upload_profile_photo',methods=['GET','POST'])
@login_required
def upload_profile_photo():
    info=None
    error=None
    form = FormPhotoProfile(request.form)
    user_name = session['id']
    count_projects = project_dao.get_projects_user_count(user_name)
    count_notifications = user_dao.get_notifications_count(user_name)


    if request.method == 'POST'  :
        if 'photo' not in request.files:
            error ='El arhivo no existe'
            return render_template('views/user/upload_profile_photo.html',form=form,info=info,error=error
            ,count_projects=count_projects,count_notifications=count_notifications)


        file = request.files['photo']
        if file.filename == '':
            error = 'Archivo no seleccionado'
            return render_template('views/user/upload_profile_photo.html',form=form,info=info,error=error
            ,count_projects=count_projects,count_notifications=count_notifications)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            user_dao.update_img_user(filename,session['id_user'])
            info = 'Se ha actualizado tu foto correctamente, debes reiniciar tu sesion para notar los cambios'
        else:
            error = 'El archivo no es compartible'



    return render_template('views/user/upload_profile_photo.html',form=form,info=info,error=error
    ,count_projects=count_projects,count_notifications=count_notifications)

@user_blueprint.route('/test',methods=['GET','POST'])
def test():

    user =  request.form['user']
    user_validate = user_dao.validate_user_exist(user)

    return "{}".format(user_validate)


@user_blueprint.route('/notify_checked',methods=['GET','POST'])
def notify_checked():
    id_notification = request.form['id']
    user_dao.notify_checked(id_notification)
    return ""


@user_blueprint.route('/delete_notification',methods=['GET','POST'])
def delete_notification():
    id_notification = request.form['id']
    user_dao.delete_notification(id_notification)
    return ""


@user_blueprint.route('/active_user',methods=['GET','POST'])
def active_user():
    active = None
    id = request.form['id']
    new_active = request.form['active']
    if (new_active == 'true'):
        active = True
    else:
        active = False
        user_dao.delete_user(id)
    user_dao.update_active_user(id,active)
    return "result"
