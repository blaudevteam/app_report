import hashlib
import io
import os

import xlrd


def get_app_base_path():
    return os.path.dirname(os.path.realpath(__file__))


def get_instance_folder_path():
    return os.path.join(get_app_base_path(), 'instance')


def checksum_file(file_byte):
    return str(hashlib.md5(file_byte.getvalue()).hexdigest())


def convert_file_to_bytes(file):
    in_memory_file = io.BytesIO()
    file.save(in_memory_file)
    return in_memory_file


def convert_file_byte_to_workbook(file_byte):
    return xlrd.open_workbook(file_contents=file_byte.getvalue())
