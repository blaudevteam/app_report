import logging
import os
from os.path import abspath, dirname

dir_path = dirname(abspath(__file__))


class BaseConfig(object):
    DEBUG = True
    TESTING = False
    SECRET_KEY = '1d94e52c-1c89-4515-b87a-f48cf3cb7f0b'
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LEVEL = logging.DEBUG
    SECURITY_CONFIRMABLE = False
    BCRYPT_LOG_ROUNDS = 15
    WTF_CSRF_ENABLED = True
    LOGGING_LOCATION = dir_path + '/../log/web_app.log'
    ITEMS_FOR_PAGE = 10
    VALUE_JOURNEY = 8
    UPLOAD_FOLDER = dir_path +'/static/img'
    UPLOAD_FOLDER_DOCUMENTS = dir_path +'/static/documents'
    SEND_FILE_MAX_AGE_DEFAULT = 0


class ProductionConfig(BaseConfig):
    DEBUG = True
    TESTING = False
    SECRET_KEY = 'a9eec0e0-23b7-4788-9a92-318347b9a39f'
    MONGO_HOST = '172.16.36.44'
    MONGO_PORT = 27017
    MONGO_DBNAME = 'report_db'
    MONGO_USERNAME = 'app_report'
    MONGO_PASSWORD = 'app_report'


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = False
    SECRET_KEY = 'a9eec0e0-23b7-4788-9a92-318347b9a39f'
    MONGO_HOST = '172.16.36.44'
    MONGO_PORT = 27017
    MONGO_DBNAME = 'report_db'
    MONGO_USERNAME = 'app_report'
    MONGO_PASSWORD = 'app_report'


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    SECRET_KEY = '792842bc-c4df-4de1-9177-d5207bd9faa6'
    MONGO_HOST = '172.16.36.44'
    MONGO_PORT = 27017
    MONGO_DBNAME = 'report_db'
    MONGO_USERNAME = 'app_report'
    MONGO_PASSWORD = 'app_report'


config = {
    "development": "web_app.config.DevelopmentConfig",
    "production": "web_app.config.ProductionConfig",
    "testing": "web_app.config.TestingConfig",
    "default": "web_app.config.DevelopmentConfig"
}


def configure_app(app, configuration='default'):
    # Configure app
    config_name = os.getenv('FLASK_CONFIGURATION', configuration)

    app.config.from_object(config[config_name])
    app.config.from_pyfile('config.cfg', silent=True)

    # Configure logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
