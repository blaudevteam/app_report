$(document).ready(function () {
  var url_server ='/report'
  var data_input = ''
  var count = 0
  $('.container_add_member').hide()
  $('.btn_add_member').hide()
  $('.btn_add_developer').hide()
  $('.panel_password').hide()
  $('.panel_password_confirm').hide()
  $('.btn_editme').hide()


  $('.active_user').children('div').children('label').click(function(event) {
      var id = $(this).parent('div').parent('div').attr('id')
      var active = $(this).parent('div').parent('div').attr('active')
      if (active=='True') {
        active = false
      }else{
        active = true
      }

      $.ajax({
        url: url_server + '/active_user',
        type: 'POST',
        data: {id: id,active:active}
      })
      .done(function(data) {
        notifi_success('Usuario actualizado',1500)
        setTimeout(function () {
  				location.reload()
        },800);
      });

  });


  $('.delete_project').click(function(event) {
    var id = $(this).attr('id');
    swal({
      title: 'Estas seguro?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!',
      cancelButtonText:'Cancelar'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: url_server + '/delete_project',
          type: 'POST',
          data: {id:id}
        })
        .done(function(data) {
          notifi_success('Se elimino el proyecto',1500)
          setTimeout(function () {
            location.reload()
          },800);
        })

      }else if (
            result.dismiss === swal.DismissReason.cancel
          ) {
            notifi_success('Tu informacion esta a salvo',1500)
          }
      })


  });

  $('.add_member').click(function(event) {
    count += 1
    if (count == 1) {
      $('.container_add_member').show('slow')
      $('.icon').removeClass('fa-plus').addClass('fa-minus')
    }else{
      $('.container_add_member').hide('slow')
      $('.icon').removeClass('fa-minus').addClass('fa-plus')
      count = 0
    }
  });

  $('.input_add_member').keyup(function(event) {
    var user_id = $(this).val()
    $.ajax({
      url: url_server + '/test',
      type: 'POST',
      data: {user:user_id},
    })
    .done(function(data) {
      if (data=="True") {
        notifi_success('Usuario encontrado',1500)
        $('.btn_add_member').show('slow')
      }else{
        $('.btn_add_member').hide('slow')
      }
    })

  });


  $('.btn_add_member').click(function(event) {
    var user_name = $('.input_add_member').val()
    var id_project = $(this).attr('id')
    $.ajax({
      url: url_server + '/add_member',
      type: 'POST',
      data: {user_name: user_name,id_project:id_project}
    })
    .done(function(data) {

      if (data=="True") {
        notifi_success('Miembro agregado',1500)
        setTimeout(function () {
          location.reload()
        },800);
      }else if (data == "rol") {
        notifi_error('El usuario es gerente','Prueba con otro usuario')
      }else if (data == "False"){
        notifi_error('El usuario ya esta agregado','Prueba con otro usuario')
      }
    })

  });

  /* VALIDATION MEMBER MATCH AND PROJECT IS PUBLIC */
  $('.show_project').click(function(event) {
    var id_project = $(this).attr('id')
    $.ajax({
      url: url_server + '/match_member_project',
      type: 'POST',
      data: {id_project: id_project}
    })
    .done(function(data) {
      if (data=="True") {
        window.location.replace(url_server + "/project/"+id_project)
      }else{

        $.ajax({
          url: url_server + '/match_admin_project',
          type: 'POST',
          data: {id_project:id_project}
        })
        .done(function(data) {
          if (data=="True" || data == true) {
            window.location.replace(url_server + "/project/"+id_project)
          }else{
            notifi_error('Por el momento no estas unido al proyecto :(','Solicita que te unan')
          }
        })
      }
    })
  });

  /* SHOW ADD MEMBER BUTTON */
  $('.add_member_btn_two').click(function(event) {
    $('.add_member').click()
  });

  /*PROCCES TASK*/

  $('.process_sprint').click(function(event) {
    var horas = 0
    var new_state = $(this).attr('dato')
    var id = $(this).attr('id')
    var id_project = $(this).attr('id_project');


    switch (new_state) {
      case 'En proceso':
        message = 'El modulo se paso a procesado'
        set_data(id,new_state,id_project,horas)
        break;

      case 'Finalizado':
        message = 'El modulo fue finalizado'

        $.ajax({
          url: url_server +'/validate_sprint_to_finished',
          type: 'POST',
          data: {id:id}
        })
        .done(function(data) {
          if (data=="False") {
            notifi_error('No se han agregado sub tareas al modulo','Debe agregar tareas para finalizarlo')
          }else{
             set_data(id,new_state,id_project,horas)
          }
        })
        break;
      default:
        message = 'No definida'
        break;
    }
  });

  function set_data(id,new_state,id_project,horas){
    $.ajax({
      url: url_server + '/update_sprint_state',
      type: 'POST',
      data: {id : id, new_state : new_state,id_project:id_project,horas:horas}
    })
    .done(function(data) {
      notifi_success(message,1500)
      setTimeout(function () {
        location.reload()
      },800);
    })
  }


    /* SHOW PROJECT DETAIL  */
    $('.label_table').click(function() {
      var data = $(this).attr('dato')
      if (data == "Detalles") {
        $('.show_menu_details').hide('slow')
      }else{
        $('.show_menu_details').show('slow')
      }
    });


    /* DELETE USER*/
    $('.delete_user').click(function(event) {
      var id = $(this).attr('id')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar',
        footer: '<a href="#">Eliminar al usuario puede afectar a los proyectos que este asociado</a>',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/delete_user',
            type: 'POST',
            data: {id: id}
          })
          .done(function(data) {
            notifi_success('El usuario fue eliminado',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })

        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });

    /* NOTIFY CHECKED */
    $('.notify_checked').click(function(event) {
      var id = $(this).attr('id')
      $.ajax({
        url: url_server + '/notify_checked',
        type: 'POST',
        data: {id:id}
      })
      .done(function() {
        notifi_success('Se actualizo la notificacion',1500)
        setTimeout(function () {
          location.reload()
        },800)
      })

    });

    /* DELETE NOTIFY */
    $('.delete_notification').click(function() {
        var id = $(this).attr('id')
        $.ajax({
          url: url_server + '/delete_notification',
          type: 'POST',
          data: {id: id}
        })
        .done(function() {
          notifi_success('Se elimino la notificacion',1500)
          setTimeout(function () {
            location.reload()
          },800)
        })

    });


    /* DELETE MEMBER */
    $('.delete_member').click(function(event) {
      var id_member = $(this).attr('id')
      var id_user = $(this).attr('id_usuario')
      var id_project = $(this).attr('id_project')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
            $.ajax({
              url: url_server + '/remove_member',
              type: 'POST',
              data: {id_member: id_member,id_user:id_user,id_project:id_project}
            })
            .done(function(data) {
              notifi_success('Se expulso al usuario',1500)
              setTimeout(function () {
                location.reload()
              },800)
            })
        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Usuario a salvo',1500)
            }
        })
    });


    /*Finish project*/
    $('.btn_finish_project').click(function(event) {
      var id = $(this).attr('id');
      $.ajax({
        url: url_server + '/finalize_project',
        type: 'POST',
        data: {id:id}
      })
      .done(function(data) {
        notifi_success('Proyecto finalizado',1500)
        setTimeout(function () {
          location.reload()
        },800)
      })

    });


    /*PROJECT TO DEVELOPMENT*/

    $('.btn_project_to_development').click(function(event) {
      id_project = $(this).attr('id');
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Regresar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/project_to_development',
            type: 'POST',
            data: {id_project:id_project}
          })
          .done(function(data) {
            notifi_success('Proyecto finalizado',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })
        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Proyecto a salvo',1500)
            }
        })
    });

    /*SHOW INPUT PASSWORD*/
    $('.show_input_password').children('div').children('div').children('label').click(function(event) {
      count +=1
      if (count == 1) {
        $('.panel_password').show('slow')
        $('.confirm_pass').val("True")
        $('.btn_editme').show('slow')
      }else{
        $('.panel_password').hide('slow')
        $('.confirm_pass').val("False")
        $('.btn_editme').hide('slow')
        count = 0
      }
    });


    /*SHOW MY PROJECTS*/
    $('.slide_my_projects').click(function(event) {
      count += 1
      if(count == 1){
        $.ajax({
          url: url_server + '/my_projects',
          type: 'POST',
          data: {}
        })
        .done(function(data) {
          var array = JSON.parse(data)
          array.map((item, index) => {
             $('.container_slide_my_projects').append('<p><a href="'+url_server+'/project/'+item['id']+'" style="color:white;text-decoration:none">'+item['nombre']+'</a></p>')
          })
          $('.container_loader').hide('slow')
        })
      }
    });

    $('.show_detail_project_home').click(function(event) {
      var id = $(this).attr('id')
      $('.container_projects_home').removeClass('col-md-12').addClass('col-md-8')
      $('.panel_task').hide('slow')
      $('.panel_task_graph').hide('slow')
      $('#panel-'+id).show('slow')
      setTimeout(function () {
        $('.bars_li-'+id).click()
      },800)
      $('#panel_graph-'+id).show('slow')
    });

    /*RELOAD */
    $('.reload').click(function(event) {
      overlay=$('<div class="load-overlay"><div><div class="c1"></div><div class="c2"></div><div class="c3"></div><div class="c4"></div></div><span>Cargando...</span></div>');
      overlay.css('opacity',1).fadeIn();
      $('.container_loader').append(overlay)
    });

    /* SHOW SPRINT DETAIL*/
    $('.show_sprint_detail').click(function(event) {
      count = 0
      var id = $(this).attr('id')
      var id_project = $(this).attr('id_project');

      $.ajax({
        url: url_server + '/match_user_module',
        type: 'POST',
        data: {id:id,id_project:id_project}
      })
      .done(function(data) {
        if (data == 'no_permitido'){
          notifi_error('No estas agregado al modulo','Solicita que te unan')
        }else{
          $('.btn_add_task').attr('ruta', url_server + '/add_task/'+id+"/"+id_project);
          $('.btn_add_task').attr('id_sprint',id);
          $('.container_detail_sprint').show('slow')

          $.ajax({
            url: url_server + '/get_detail_sprint',
            type: 'POST',
            data: {id:id}
          })
          .done(function(data) {
            var array = JSON.parse(data)
            var name = array.nombre
            var date_estimated = array.fecha_estimada
            var time_estimated = parseFloat(array.horas_estimadas)
            var flag_client = array.flag_cliente
            var journeys = array.jornada
            var state = array.estado


            if(state == "Finalizado"){
              $('.btn_add_task').hide()
            }else{
              $('.btn_add_task').show()
            }

            if (flag_client  == true){
              if(journeys > 1 || journeys == 0){
                text_hour = "jornadas"
              }else{
                text_hour = "jornada"
              }
              time_module = journeys
            }else{
              if(time_estimated > 1 || time_estimated == 0){
                text_hour = "horas"
              }else{
                text_hour = "hora"
              }
              time_module = time_estimated
            }


            $('.title_sprint').html('<strong>Modulo </strong> '+name)
            $('.date_sprint').text(date_estimated)
            $('.time_sprint').text(time_module + ' ' + text_hour)
            $.ajax({
              url: url_server + '/get_tasks_sprint',
              type: 'POST',
              data: {id:id}
            })
            .done(function(data) {
              $('.container_list_sprint').html('')
              var array = JSON.parse(data)
              if (array != "") {
                array.map((item, index) => {
                  count += 1
                  if (parseFloat(item['horas_invertidas']) > 1 || parseFloat(item['horas_invertidas']) == 0 ){
                    text_time = 'horas'
                  }else{
                    text_time = 'hora'
                  }
                  if(count == 1){
                    position = 'left'
                    color = 'primary'
                  }else{
                    position = 'right'
                    color = 'darkorange'
                    count = 0
                  }
                  var id_sprint = "'"+id+"'"
                  var id_task_item = "'"+item['id']+"'"
                  var id_project_item = "'"+id_project+"'"
                  var url_server_item = "'"+url_server+"'"
                  $('.container_list_sprint').append('<li class="'+position+'  flip-bg-white bd-'+color+' col-md-4"><section class="bg-'+color+'"><time style="color:white"><i class="fa fa-clock-o"></i>'+item['horas_invertidas']+' '+text_time+'</time><h3>'+item['nombre']+'</h3>'+item['comentario']+'<br><p> - Por '+item['nombre_usuario']+'</p><button href="#" style="float:right" class="btn btn-danger" onClick="test_delete('+id_task_item+','+id_project_item+','+url_server_item+','+id_sprint+')" ><i class="fa fa-times"></i></button></section></li>')
                })
              }else{
                $('.container_list_sprint').append('<h1 class="text-center">El modulo no contiene tareas aun</h1>')
              }
            })
          })
        }
      })



    });


    $('.hide_detail').click(function(event) {
      $('.container_detail_sprint').hide('slow')
      $('.container_assign_developer').hide('slow')
      $('.container_list_developers').hide('slow')
      $('.container_list_sprint').html('')
      $('.container_documents').hide('slow')
    });

    /*ADD TASK*/
    $('.btn_add_task').click(function(event) {
      var id_sprint = $(this).attr('id_sprint')
      var ruta = $(this).attr('ruta')
      $.ajax({
        url: url_server +'/match_developer_sprint',
        type: 'POST',
        data: {id_sprint: id_sprint}
      })
      .done(function(data) {
        if (data== "True") {
          window.location.replace(ruta)
        }else if (data=="False") {
          notifi_error('No estas asignado a este modulo','Para agrega tareas, debes estar agregado')
        }
      })
    });

    /*DELETE SPLIT*/
    $('.delete_sprint').click(function(event) {
      var id = $(this).attr('id')
      var id_project = $(this).attr('id_project')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar',
        footer: '<a href="#">Eliminar el modulo puede afectar las horas del proyecto</a>',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server +'/delete_sprint',
            type: 'POST',
            data: {id:id,id_project:id_project}
          })
          .done(function(data) {
            notifi_success('Modulo eliminado con exito',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })
        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });

    /* ASSIGN DEVELOPER*/
    $('.assign_developer').click(function(event) {
      $('.btn_add_developer').attr('name',$(this).attr('name'))
      $('.btn_add_developer').attr('id_sprint',$(this).attr('id'))
      $('.btn_add_developer').attr('id_project',$(this).attr('id_project'))
      $('.container_assign_developer').show('slow')
    });

    $('.input_add_developer').keyup(function(event) {
      var user_id = $(this).val()
      $.ajax({
        url: url_server + '/test',
        type: 'POST',
        data: {user:user_id},
      })
      .done(function(data) {
        if (data=="True") {
          notifi_success('Usuario encontrado',1500)
          $('.btn_add_developer').show('slow')
        }else{
          $('.btn_add_developer').hide('slow')
        }
      })
    });

    $('.btn_add_developer').click(function(event) {
      var user_name = $('.input_add_developer').val()
      var id_sprint = $(this).attr('id_sprint')
      var id_project = $(this).attr('id_project')
      var modulee = $(this).attr('name')

      $.ajax({
        url: url_server +'/add_developer_to_project',
        type: 'POST',
        data: {user_name:user_name,id_sprint:id_sprint,modulee:modulee,id_project:id_project}
      })
      .done(function(data) {
        if (data == "asignar"){
          notifi_success('Modulo asignado',1500)
          setTimeout(function () {
            location.reload()
          },800)
        }else if (data == "rol") {
          notifi_error('El usuario es un gerente','Debe ingresar a un developer o a un gestor')
        }else if (data == "asignado") {
          notifi_error('El developer ya esta agregado','Prueba con otro usuario')
        }else if (data == "miembro") {
          notifi_error('El developer no esta asignado al proyecto','Debe asignarlo al proyecto')
        }
      })
    });

    /* SHOW LIST DEVELOPERS*/
    $('.btn_list_developers').click(function(event) {
      var id = $(this).attr('id')
      var id_project = $(this).attr('id_project')
      $('.container_list_developers').html(' ')
      $('.container_list_developers').show('slow')
      $.ajax({
        url: url_server +'/get_developers_sprint',
        type: 'POST',
        data: {id:id}
      })
      .done(function(data) {
        var array = JSON.parse(data)
        if (array != "") {
          array.map((item, index) => {
             var id_sprint = "'"+item['id_sprint']+"'"
             var id_user = "'"+item['id_usuario']+"'"
             var url_server_item = "'"+url_server+"'"
             var item_project_id = "'"+id_project+"'"
             $('.container_list_developers').append('<div class="col-md-4 " style="margin-top:10px;"><section class="panel " style="box-shadow: 0px 0px 1px #000"><header class="panel-heading"><h3 class="">'+item['usuario']+'</h3></header><div class="panel-tools half-line circle" align="right"><ul class="tooltip-area"><li><a href="javascript:void(0)" class="btn btn-danger" onClick="delete_developer_sprint('+id_sprint+','+id_user+','+url_server_item+','+item_project_id+')" title="Expulsar"><i class="fa fa-times"></i></a></li></ul></div></section></div>')
          })
        }else{
          $('.container_list_developers').append('<h1 class="text-center" style="margin-top:20px">No hay developers agregados</h1>')
        }
      })

    });


    /*SHOW BTN EDITME*/
    $('.email_editme').click(function(event) {
      data_input = $(this).val()
    });

    $('.email_editme').keyup(function(event) {
      this_input_data = $(this).val()
      if (this_input_data == data_input){
        $('.btn_editme').hide('slow')
      }else{
        $('.btn_editme').show('slow')
      }
    });


    /*PROCESS PROJECT*/
    $('.process_project').click(function(event) {
      var id = $(this).attr('id')
      var id_admin = $(this).attr('admin')

      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/update_project_to_development',
            type: 'POST',
            data: {id_project:id}
          })
          .done(function(data) {
            notifi_success('Proyecto en desarrollo',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })
        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('El proyecto sigue pendiente',1500)
            }
        })


    });

    /* DELETE CLIENT */
    $('.delete_client').click(function(event) {
      var id = $(this).attr('id')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/delete_client',
            type: 'POST',
            data: {id: id}
          })
          .done(function(data) {
            notifi_success('El cliente fue eliminado',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })

        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });

    /* SELECT COMPANY*/
    $('.select_company').change(function(event) {
        var id_company = $(this).val()
        $('.input_company').val(id_company)
        validate_input_client()
    });

    $('.select_group').change(function(event) {
        var id_group = $(this).val()
        $('.input_group').val(id_group)
        validate_input_client()
    });

    /* SELECTS NEW ASSIGNMENT*/
    $('.select_group_assign').change(function(event) {
        var id_group = $(this).val()
        $('.input_group_assign').val(id_group)
        validate_input_assign()
    });

    $('.select_project_assign').change(function(event) {
        var id_project = $(this).val()
        $('.input_project_assign').val(id_project)
        validate_input_assign()
    });


    /* CHANGE VALUE EDIT NAME COMPANY*/
    $('.input_name_company_edit').keyup(function(event) {
      var input = $(this).val()
      var input_copy = $('.input_name_company_edit_copy').val()

      if (input == input_copy){
        $('.btn_edit_company').hide('slow')
      }else{
        $('.btn_edit_company').show('slow')
      }

    });

    /* CHANGE VALUE EDIT NAME GROUP*/
    $('.input_name_group_edit').keyup(function(event) {
      var input = $(this).val()
      var input_copy = $('.input_name_group_edit_copy').val()

      if (input == input_copy){
        $('.btn_edit_group').hide('slow')
      }else{
        $('.btn_edit_group').show('slow')
      }

    });

    /*DELETE CLIENT*/
    $('.delete_company').click(function(event) {
      var id = $(this).attr('id')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/delete_company',
            type: 'POST',
            data: {id: id}
          })
          .done(function(data) {
            notifi_success('La empresa fue eliminada',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })

        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });

    /*DELETE GROUP*/
    $('.delete_group').click(function(event) {
      var id = $(this).attr('id')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar',
        footer: '<a href="#">Eliminar al grupo afectara a las asignaciones</a>',
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/delete_group',
            type: 'POST',
            data: {id: id}
          })
          .done(function(data) {
            notifi_success('El grupo fue eliminado',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })

        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });

    /* DELETE ASSIGNMENT*/
    $('.delete_assignment').click(function(event) {
      var id = $(this).attr('id')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/delete_assignment',
            type: 'POST',
            data: {id: id}
          })
          .done(function(data) {
            notifi_success('La asignacion fue eliminada',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })

        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });


    /*ACTIVE TICKET*/
    $('.active_ticket').children('div').children('label').click(function(event) {
        var id = $(this).parent('div').parent('div').attr('id')
        var active = $(this).parent('div').parent('div').attr('active')

        if (active=='True') {
          active = false
        }else{
          active = true
        }

        $.ajax({
          url: url_server + '/active_ticket',
          type: 'POST',
          data: {id: id,active:active}
        })
        .done(function(data) {
          notifi_success('Asignacion actualizada',1500)
          setTimeout(function () {
    				location.reload()
          },800);
        });

    });

    /* DELETE SPRINT PROJECT*/
    $('.delete_sprint_project').click(function(event) {
      var id = $(this).attr('id')
      var id_project = $(this).attr('id_project')
      swal({
        title: 'Estas seguro?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText:'Cancelar'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: url_server + '/delete_sprint_project',
            type: 'POST',
            data: {id: id,id_project:id_project}
          })
          .done(function(data) {
            notifi_success('El sprint fue eliminado',1500)
            setTimeout(function () {
              location.reload()
            },800)
          })

        }else if (
              result.dismiss === swal.DismissReason.cancel
            ) {
              notifi_success('Tu informacion esta a salvo',1500)
            }
        })
    });

    /* SELECT SPRINT*/
    $('.select_sprint_project').change(function(event) {
        var id_sprint = $(this).val()
        $('.input_sprint_project').val(id_sprint)
        if (id_sprint != ''){
          $('.btn_sprint_project').show('slow')
        }else{
          $('.btn_sprint_project').hide('slow')
        }
    });

    /* COMMENTS*/

    /*SHOW COMMENTS*/
    $('.coments_module').click(function(event) {
      var id = $(this).attr('id')
      var id_project = $(this).attr('id_project')
      var id_user = $('.label_id_user').attr('id')
      $('.btn_input_module').attr('id', id)
      $('.container_chat_module').html('')

      $.ajax({
        url: url_server+'/match_user_module',
        type: 'POST',
        data: {id:id,id_project:id_project}
      })
      .done(function(data) {
        if (data == 'no_permitido'){
          $('.panel_comments_module').hide('slow')
          notifi_error('No estas agregado al modulo','Solicita que te unan')
        }else{
          $('.panel_comments_module').show('slow')
          if(data=='gerente'){
            $('.input_comment_module').hide('slow')
            $('.btn_input_module').hide('slow')
          }
          $.ajax({
            url: url_server + '/get_comments_module',
            type: 'POST',
            data: {id:id}
          })
          .done(function(data) {
            var array = JSON.parse(data)
            if (array != "") {
              array.map((item, index) => {
                var id_moduel = "'"+item['id']+"'"
                var url_serve = "'"+url_server+"'"
                if (item['usuario'] == id_user){
                  $('.container_chat_module').append('<dt id="time_comment-'+item['id']+'" style="display:none"><strong></strong> '+item['creacion']+'</dt><dd edit="'+item['editado']+'" id="text_comment-'+item['id']+'" class="to" onClick="show_detail_comment('+id_moduel+','+url_serve+')"  style="cursor:pointer"><p>'+item['mensaje']+'</p></dd>')
                }else{
                  $('.container_chat_module').append('<dt id="time_comment-'+item['id']+'" style="display:none"><strong></strong> '+item['creacion']+'</dt><dd edit="'+item['editado']+'" id="text_comment-'+item['id']+'" class="from" onClick="show_detail_comment('+id_moduel+','+url_serve+')"  style="cursor:pointer;margin-top:15px"><p>'+item['mensaje']+'</p><img alt="" src="'+url_server+'/static/img/'+item['img_user']+'" class="avatar circle"></dd>')
                }
              })
            }else{
              $('.container_chat_module').append('<h3 style="text-align:center">Sin comentarios</h3>')
            }
            validate_new_comment_module(id,url_server)
          })
        }
      })


    });

    /*ACTIVE BUTTON TO ADD COMMENT*/
    $('.input_comment_module').keyup(function(event) {
      var this_value = $(this).val()
      if(this_value != ''){
          $('.btn_input_module').attr('disabled', false)
      }else{
        $('.btn_input_module').attr('disabled', true)
      }
    });

    /*ADD COMMENT*/
    $('.btn_input_module').click(function(event) {
      var message = $('.input_comment_module').val()
      var id_module = $(this).attr('id')
      $('.btn_input_module').attr('disabled', true)
      $.ajax({
        url: url_server + '/add_comment_module',
        type: 'POST',
        data: {id_module:id_module,message:message}
      })
      .done(function() {
        count +=1
        var d = new Date()
        var currentdate = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()
        var id = "'"+count+"'"
        $('.container_chat_module').append('<dt id="time_comment_now-'+count+'" style="display:none"><strong></strong> '+currentdate+'</dt><dd class="to" onClick="show_timme_comment('+id+')" style="cursor:pointer"><p>'+message+'</p></dd>')
        $('.input_comment_module').val('')
      })
    });

    /*SHOW DETAIL COMMENT CLIENT*/
    $('.detail_comment').click(function(event) {
      var id = $(this).attr('id')
      $('#text-detail-'+id).show('slow')
    });

    /*START LISTENER COMMENT*/
    $('.find_new_comments_client').click(function(event) {
      var id = $(this).attr('id')
      validate_new_comment_module(id,url_server)
    });


    /*CHANGE TIME MODULE*/
    $('.change_journeys_hours').children('div').children('label').click(function(event) {
      count +=1
      if (count == 1){
        $('.title_journeys_hours').text('Jornadas')
        $('.input_hours_moduel').hide('slow')
        $('.input_journeys_moduel').show('slow')
        $('.input_hours_moduel').val(0)
        $('.input_flag_journeys').val(1)
      }else{
        $('.input_journeys_moduel').hide('slow')
        $('.input_hours_moduel').show('slow')
        $('.input_journeys_moduel').val(0)
        $('.input_flag_journeys').val(0)
        $('.title_journeys_hours').text('Horas')
        count = 0
      }
    });

    /*EDIT TIME MODULE*/
    $('.switch_edit_time_module').children('div').children('label').click(function(event) {
      var flag_journeys = $('.input_flag_journeys').val()
      if(flag_journeys == 0){
        $('.input_edit_time_module').hide('slow')
        $('.input_edit_time_module').val(0)
        $('.input_edit_journey_module').show('slow')
        $('.tittle_edit_time_module').text('Jornadas')
        $('.input_flag_journeys').val(1)
      }else{
        $('.input_edit_journey_module').hide('slow')
        $('.input_edit_journey_module').val(0)
        $('.input_edit_time_module').show('slow')
        $('.tittle_edit_time_module').text('Horas')
        $('.input_flag_journeys').val(0 )
      }
    });

    /*SHOW EDIT PASSORD CLIENT*/
    $('.show_input_password_client').children('div').children('label').click(function(event) {
      count +=1
      if(count==1){
        $('.input_pass_profile').show('hide')
        $('.input_flag_password').val(1)
      }else{
        $('.input_pass_profile').hide('hide')
        $('.input_flag_password').val(0)
        count=0
      }
    })

    /*SHOW DOCUMENTS*/
    $('.show_documents_module').click(function(event) {
      $('.container_documents_detail').html(' ')
      var id = $(this).attr('id')
      var id_project = $(this).attr('id_project')
      var state = $(this).attr('estado')
      if (state == 'Finalizado'){
        $('.btn_redirect_document_module').hide()
      }
      $.ajax({
        url: url_server+'/match_user_module',
        type: 'POST',
        data: {id:id,id_project:id_project}
      })
      .done(function(data) {
          if (data == 'no_permitido'){
            notifi_error('No estas agregado al modulo','Solicita que te unan')
          }else{
            $('.btn_redirect_document_module').attr('href', url_server + '/add_docuemnt/'+id+'/'+id_project)
            $('.container_documents').show('slow')
            $.ajax({
              url: '/get_documents_module',
              type: 'POST',
              data: {id:id}
            })
            .done(function(data) {
              var array = JSON.parse(data)
              if (array != "") {
                array.map((item, index) => {
                  if (item['extension'] == 'png' || item['extension'] == 'jpg' || item['extension'] == 'jpeg' || item['extension'] == 'PNG'){
                    img = 'imagen.png'
                  }else if (item['extension'] == 'docx' || item['extension'] == 'docm' || item['extension'] == 'dotx' || item['extension'] == 'doc') {
                    img = 'word.png'
                  }else if (item['extension'] == 'xlsx' || item['extension'] == 'xlsm' || item['extension'] == 'xls' || item['extension'] == 'csv') {
                    img = 'excel.png'
                  }else if (item['extension'] == 'pdf') {
                    img = 'pdf.ico'
                  }else{
                    img = 'not_found.png'
                  }
                  var url_serve = ""+url_server+""
                  var id_document = ""+item['id']+""
                  $('.container_documents_detail').append('<a href=" '+url_serve+'/download_document/'+id_document+'"><div class="col-sm-4" style="cursor:pointer"><img alt="" src="'+url_server+'/static/img/'+img+'" class="img-document" ><p style="text-align:center">'+item['nombre']+'</p></div></a>')
                })
              }else{
                $('.container_documents_detail').append('<h2 style="text-align:center" >No hay documentos</h2>')
              }
            })
          }
      })

    });

})

function show_timme_comment(id){
  $('#time_comment_now-'+id).show('slow')
}

function show_detail_comment(id,url_server) {
  var edit = $('#text_comment-'+id).attr('edit')
  $('#time_comment-'+id).show('slow')
}



function validate_new_comment_module(id,url_server) {
  var id_user = $('.label_id_user').attr('id')
  $.ajax({
    url: url_server + '/validate_new_comment_module',
    type: 'POST',
    data: {id:id}
  })
  .done(function(data) {
    var array = JSON.parse(data)
    var label_id = $('.label_id_comment').attr('id')
    if (array != "") {
      array.map((item, index) => {
        if (item['usuario'] != id_user){
          if(item['id'] != label_id && label_id != ""){
            var id_moduel = "'"+item['id']+"'"
            var url_serve = "'"+url_server+"'"
            $('.container_chat_module').append('<dt id="time_comment-'+item['id']+'" style="display:none"><strong></strong> '+item['creacion']+'</dt><dd class="from" edit="'+item['editado']+'" id="text_comment-'+item['id']+'" onClick="show_detail_comment('+id_moduel+','+url_serve+')" style="cursor:pointer;margin-top:15px"><p>'+item['mensaje']+'</p><img alt="" src="'+url_server+'/static/img/'+item['img_user']+'" class="avatar circle"></dd>')
          }
          $('.label_id_comment').attr('id',item['id'])
        }
      })
    }
    validate_new_comment_module(id,url_server)
  })
}

function validate_input_client(){
  var company = $('.input_company').val()
  var group = $('.input_group').val()
  if(company != '' && group != ''){
    $('.btn_add_client').show('slow')
  }else{
    $('.btn_add_client').hide('slow')
  }
}

function validate_input_assign(){
  var project = $('.input_project_assign').val()
  var group = $('.input_group_assign').val()
  if(project != '' && group != ''){
    $('.btn_add_assign').show('slow')
  }else{
    $('.btn_add_assign').hide('slow')
  }
}

function test_delete(id,id_project,url_server,id_sprint) {
  swal({
    title: 'Estas seguro?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminar!',
    cancelButtonText:'Cancelar'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: url_server + '/delete_task',
        type: 'POST',
        data: {id: id,id_project:id_project,id_sprint:id_sprint}
      })
      .done(function(data) {
        if (data=="True"){
          notifi_success('La tarea fue eliminada',1500)
          setTimeout(function () {
            location.reload()
          },800)
        }else if(data=="permiso_denegado"){
          notifi_error('No tiene permisos para eliminar la tarea','Solo los developers pueden eliminar tareas')
        }else if(data=="sprint_finalizada"){
          notifi_error('No se puede eliminar la tarea','El modulo ya ha sido finalizado')
        }else if (data == "no_asignado") {
          notifi_error('No estas asignado a este modulo','Debes estar asignado al modulo para eliminar la tarea')
        }
      })
    }else if (
          result.dismiss === swal.DismissReason.cancel
        ) {
          notifi_success('Tu informacion esta a salvo',1500)
        }
    })
}


function delete_developer_sprint(id_sprint,id_user,url_server,id_project) {
  swal({
    title: 'Estas seguro?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminar!',
    cancelButtonText:'Cancelar'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: url_server + '/delete_develop_sprint',
        type: 'POST',
        data: {id_sprint:id_sprint,id_user:id_user,id_project:id_project}
      })
      .done(function() {
        notifi_success('Developer expulsado de la tarea',1500)
        setTimeout(function () {
  				location.reload()
        },900);
      })

    }else if (
          result.dismiss === swal.DismissReason.cancel
        ) {
          notifi_success('Tu informacion esta a salvo',1500)
        }
    })
}



function notifi_success(message,time) {
  swal({
    position: 'top-end',
    type: 'success',
    title: message,
    showConfirmButton: false,
    timer: time
  })
}

function notifi_error(message,foot) {
  swal({
    type: 'error',
    title: 'Oops...',
    text: message,
    footer: '<a href="#">'+foot+'</a>',
  })
}
