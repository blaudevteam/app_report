$(document).ready(function () {
  var url_server ='/report'

  $(".user_id").keyup(function () {
    var user_id = $(this).val()
    $.ajax({
      url: url_server + '/test',
      type: 'POST',
      data: $('.form_new_user').serialize(),
    })
    .done(function(data) {
      if (data=="True") {
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'El usuario ya existe',
          footer: '<a href="#">Prueba con otro nombre</a>',
        })
      }
    })
  })

  $(".client_id").keyup(function () {
    var user_id = $(this).val()
    $.ajax({
      url: url_server + '/validate_client_exist',
      type: 'POST',
      data: $('.form_new_user').serialize(),
    })
    .done(function(data) {
      if (data=="True") {
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'El usuario ya existe',
          footer: '<a href="#">Prueba con otro nombre</a>',
        })
      }
    })
  })


})
